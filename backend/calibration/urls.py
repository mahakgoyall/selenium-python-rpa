"""calibration URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import settings
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns
from session.views import GO,Submit,Rollback,Delete,Setting,email_updates
urlpatterns = [
    path('api_go/', GO.as_view()),
    path('api_submit/', Submit.as_view()),
    path('api_rollback/', Rollback.as_view()),
    path('api_delete/', Delete.as_view()),
    path('api_settings/', Setting.as_view()),
    path('api_email/', email_updates.as_view()),
    path('admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
