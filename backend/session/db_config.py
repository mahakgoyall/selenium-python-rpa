import pandas as pd
import sys
import urllib.parse
import sqlalchemy
import yagmail
import configparser
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from os import listdir
from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException, ElementNotVisibleException, \
    WebDriverException
from selenium.webdriver.common.keys import Keys
import time
from bs4 import BeautifulSoup
import sys
import csv
import os
import logging
from logging.config import fileConfig

def error_logging():
    fileConfig(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'logging_config.ini'))
    logger = logging.getLogger()
    return logger


# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))
# config.read('config.ini')

# yagmail setup
# yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
# yag = yagmail.SMTP(str(config['mail']['sender_mail']))


def connection_open():
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) + "};SERVER=" + str(
        config['connection']['server']) + ";DATABASE=" + str(config['connection']['database']) + ";UID=" + str(
        config['connection']['uid']) + ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine

def main_func():
    logger = error_logging()
    logger.info('database configuration starts')
    engine = connection_open()
    # con = engine.connect()
    # trans = con.begin()
    # con.execute("Truncate table CSV_DATA_NEW")
    # trans.commit()
    # raw_file = pd.read_csv(str(config['files']['path_to_dataFile']))
    # raw_file_na = raw_file.fillna('')
    # col_list = raw_file_na.columns.get_values()
    # col_list_str = '('
    # for col in col_list:
    #     col_list_str += '[' + str(col) + '],'
    # col_list_final = col_list_str[:-1]
    # col_list_final += ')'
    # temp_str = '('
    # print(len(raw_file_na))
    # count = 0
    # for index, row in raw_file_na.iterrows():
    #     count += 1
    #     for col in col_list:
    #         temp_str += "'" + str(row[col]).replace("'", "''") + "',"
    #     temp_str = temp_str[:-1]
    #     temp_str += '),('
    #     if count % 500 == 0: 
    #         print(count)
    #         bulk_query = "insert into CSV_DATA_NEW " + col_list_final + " values " + str(temp_str[:-2])
    #         con = engine.connect()
    #         trans = con.begin()
    #         con.execute(bulk_query)
    #         trans.commit()
    #         temp_str = '('
    # else:
    #     print(index, "Inside else")
    #     bulk_query = "insert into CSV_DATA_NEW " + col_list_final + " values " + str(temp_str[:-2])
    #     con = engine.connect()
    #     trans = con.begin()
    #     con.execute(bulk_query)
    #     trans.commit()
    # # with engine.connect() as conn, conn.begin() as trans:
    # #     raw_file_na.to_sql('CSV_DATA_NEW', conn, if_exists='replace', index=False)
    # # trans.commit()
    # print('raw data pushed to db table.')
    #store hierarchy file
    con = engine.connect()
    trans = con.begin()
    rs = con.execute("exec hierarchy_proc;")
    trans.commit()
    hierarchy_data = pd.read_sql_query("select * from hierarchy_tbl", engine)
    hierarchy_data.to_csv('files//hierarchy//hierarchy_file.csv', index=False)
    print("hierarchy table pushed into csv")
    #store level1 file
    con = engine.connect()
    trans = con.begin()
    rs = con.execute("exec level1_proc;")
    trans.commit()
    level1_data = pd.read_sql_query("select * from level1_tbl", engine)
    level1_data.to_csv('files//LEVEL1//level1.csv', index=False)
    #store level2 files
    con = engine.connect()
    trans = con.begin()
    rs = con.execute("exec level2_proc;")
    trans.commit()
    level2_data = pd.read_sql_query("select * from level2_tbl", engine)
    level2_data.to_csv('files//LEVEL2//level2.csv', index=False)
    #level3 and level4 formed by calling single procedure
    con = engine.connect()
    trans = con.begin()
    rs = con.execute("exec fetching_userid_L3_L4;")
    trans.commit()
    level3_data = pd.read_sql_query("select * from level3_tbl", engine)
    level3_data.to_csv('files//LEVEL3//level3.csv', index=False)
    level4_data = pd.read_sql_query("select * from level4_tbl", engine)
    level4_data.to_csv('files//LEVEL4//level4.csv', index=False)
    #logic for GMC files
    con = engine.connect()
    trans = con.begin()
    rs = con.execute("exec populating_GMC_info;")
    trans.commit()
    con = engine.connect()
    trans = con.begin()
    GMC_names = con.execute("select * from GMC_tables_created").fetchall()
    for GMC_name in GMC_names:
        con = engine.connect()
        trans = con.begin()
        print('gmc names: ', GMC_name[0])
        GMC_table = pd.read_sql_query("select * from " + str(GMC_name[0]), engine)
        GMC_table.to_csv('files//GMC//'+str(GMC_name[0])+'.csv', index=False)

    #truncate logging tables
    con = engine.connect()
    trans = con.begin()
    rs = con.execute("truncate table Processing_Status; truncate table Finalizing_Status;")
    trans.commit()

    #insert the guidelines and email updates flag into the db table
    con = engine.connect()
    trans = con.begin()
    rs = con.execute("insert into Finalizing_Status values ('null', 'guidelines', 'null'), ('null', 'email_updates', '0')")
    trans.commit()
    print('done')

if __name__ == '__main__':
    main_func()
