import sys
import urllib.parse
import sqlalchemy
import yagmail
import configparser
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from os import listdir
from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException, ElementNotVisibleException, \
    WebDriverException
from selenium.webdriver.common.keys import Keys
import time
from bs4 import BeautifulSoup
import sys
import csv
import os
import logging
from logging.config import fileConfig
from .send_email import send_email

# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))


def connection_open():
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) + "};SERVER=" + str(
        config['connection']['server']) + ";DATABASE=" + str(config['connection']['database']) + ";UID=" + str(
        config['connection']['uid']) + ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine


# yagmail setup
# yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
# yag = yagmail.SMTP(str(config['mail']['sender_mail']))
# mail_list = config['mail']['receiver_mail'].split(',')
# mail_list_tuple = tuple((e.strip() for e in mail_list))


def filter_setup(status, browser):
    # clear filter before setting it up.
    select_filter_var = None
    select_filter_var_count = 5
    while not select_filter_var and select_filter_var_count > 0:
        try:
            select_filter_var_count -= 1
            select_filter_var = browser.find_elements_by_tag_name('span')
            for span in select_filter_var:
                if span.text.lower().strip() == 'filter' or span.text.lower().strip() == 'filter(1)':
                    span.click()
                    break
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not select_filter_var and select_filter_var_count <= 0:
        yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find the filter on manage calibration page')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)

    clear_filter = None
    clear_filter_count = 5
    while not clear_filter and clear_filter_count > 0:
        try:
            clear_filter_count -= 1
            clear_filter = browser.find_element_by_xpath('//button[text()="Clear"]')
            clear_filter.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not clear_filter and clear_filter_count <= 0:
        yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find clear filter entity in filter pop up.')
        sys.exit(str(config['system']['exit_code']))


def main_func():
    print('It worked delete')
    # logger = error_logging()
    engine = connection_open()
    # logger.info("entered in main function.")

    try:
        browser = webdriver.Chrome(executable_path=str(config['system']['crome_driver_path']))
    except WebDriverException as e:
        # logger.error(str(e))
        yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']), contents=str(e))
        sys.exit(str(config['system']['exit_code']))  # Type valid exit code

    browser.get(str(config['system']['web_url']))
    browser.maximize_window()

    username_var = None
    username_find_count = 5
    # logger.debug("Trying to find username section")
    while not username_var and username_find_count > 0:
        try:
            username_find_count -= 1
            username_var = browser.find_element_by_id('username')
            username_var.send_keys(str(config['system']['login_username']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not username_var and username_find_count <= 0:
        yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find username field in login page')
        sys.exit(str(config['system']['exit_code']))

    password_var = None
    password_find_count = 5
    while not password_var and password_find_count > 0:
        try:
            password_find_count -= 1
            password_var = browser.find_element_by_id('password')
            password_var.send_keys(str(config['system']['login_password']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)

    if not password_var and password_find_count <= 0:
        yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find password field in login page')
        sys.exit(str(config['system']['exit_code']))

    login_link = None
    login_link_count = 5
    while login_link_count > 0 and not login_link:
        try:
            login_link_count -= 1
            login_link = browser.find_element_by_name('login')
            login_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if login_link_count <= 0 and not login_link:
        yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find login button link in login page')
        sys.exit(str(config['system']['exit_code']))

    home_click = None
    home_click_count = 5
    while home_click_count > 0 and not home_click:
        try:
            home_click_count -= 1
            home_click = browser.find_element_by_id('customHeaderModulePickerBtn-content')
            home_click.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if home_click_count <= 0 and not home_click:
        yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find home button attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    admin_center = None
    admin_center_count = 5
    while not admin_center and admin_center_count > 0:
        try:
            admin_center_count -= 1
            admin_center = browser.find_element_by_link_text('Admin Centre')
            admin_center.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not admin_center and admin_center_count <= 0:
        yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find admin center attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    manage_calibration = None
    manage_calibration_count = 5
    while not manage_calibration and manage_calibration_count > 0:
        try:
            manage_calibration_count -= 1
            manage_calibration = browser.find_element_by_id("40_")
            manage_calibration.send_keys("Manage Compensation Bucketing Sessions")
            manage_calibration.send_keys(Keys.ARROW_DOWN)
            manage_calibration.send_keys(Keys.ENTER)
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not manage_calibration and manage_calibration_count <= 0:
        yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t validate manage calibration entity on home page')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)
    filter_setup('Setup', browser)
    time.sleep(2)

    delete_session = True
    while delete_session:
        try:
            time.sleep(2)
            delete_session = browser.find_element_by_xpath('//*[@title="Delete Session"]')
            # for elem in delete_session:
            print("attribute: ", str(delete_session))
            delete_session.click()
            pop_click = browser.find_element_by_class_name(' globalPrimaryButton')
            pop_click.click()
            browser.refresh()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)


##logger.info("Done!")

if __name__ == '__main__':
    main_func()
