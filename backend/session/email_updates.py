# coding: utf-8
import pandas as pd
import yagmail
from tabulate import tabulate
import sys
import csv
import os
import logging
from logging.config import fileConfig
import glob
import shutil
import sys
import urllib.parse
import sqlalchemy
import yagmail
from os import listdir
import time
import configparser
from .send_email import send_email


def error_logging():
    fileConfig(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'logging_config.ini'))
    logger = logging.getLogger()
    return logger


# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

# yagmail setup
# yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
# yag = yagmail.SMTP(str(config['mail']['sender_mail']))
# mail_list = config['mail']['receiver_mail'].split(',')
# mail_list_tuple = tuple((e.strip() for e in mail_list))

def connection_open():
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) +
                                "};SERVER=" + str(config['connection']['server']) + ";DATABASE=" +
                                str(config['connection']['database']) + ";UID=" + str(config['connection']['uid']) +
                                ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine


def without_gmc(master, new, _id, data3, mail_list):
    # Without GMC
    # Fetch maximum level
    max_level_master = master['Session Name'].str.split(' ').str[2].str[1]
    max_level_new = new['Session Name'].str.split(' ').str[2].str[1]

    # If max level for master and new is not equal than return
    if max_level_master.max() != max_level_new.max():
        return

    # Get max level value for master and new
    max_level_master = max_level_master.max()
    max_level_new = max_level_new.max()

    # Append level column in master and new for level
    master['level'] = master['Session Name'].str.split(' ').str[2].str[1]
    new['level'] = new['Session Name'].str.split(' ').str[2].str[1]

    # Check the status of max of master level and max of new level
    master_status = master[master['level'] == max_level_master]['Session Status'].values.tolist()[0].lower()
    new_status = new[new['level'] == max_level_new]['Session Status'].values.tolist()[0].lower()

    # If master_status is in progress and new_status if approved than check the ratings
    if master_status == 'In Progress'.lower() and new_status == 'Approved'.lower():

        # Fetch rating of "master - 1" and "new"
        rating_master = master[master['level'] == str(int(max_level_master) - 1)]['New Value Label'].values.tolist()[0]
        rating_new = new[new['level'] == max_level_new]['New Value Label'].values.tolist()[0]

        if rating_master != rating_master or rating_new != rating_new:
            return

        # If rating of master and rating of new is equal than return
        if rating_master == rating_new:
            return

        # Search in hierarchy table for the user id and level
#         mgr_email = data3[(data3['levels'] == (int(max_level_master) - 1)) &
#                           (data3['UserId'] == str(_id))]['mgr_email'].values.tolist()[0]

        # Fetch session name of Max level - 1
        max_level_1_session_name = master[master['level'] == (str(int(max_level_master)
                                                                  - 1))]['Session Name'].values.tolist()[0]
        max_level_1_session_mgr_id = max_level_1_session_name.split('_')[1]

        max_level_session_name = master[master['level'] == str(max_level_master)]['Session Name'].values.tolist()[0]
        max_level_session_mgr_id = max_level_session_name.split('_')[1]

        if max_level_1_session_mgr_id == max_level_session_mgr_id:
            return

        mgr_email = data3[data3['mgr_id'] == max_level_1_session_mgr_id]['mgr_email'].values.tolist()[0]

        #
        if mgr_email in mail_list:
            print("without_gmc append")
            mail_list[mgr_email].append([_id, master['Emp Name  First Name'].values.tolist()[0] + ' ' +
                                     master['Emp Name  Last Name'].values.tolist()[0],
                                    rating_new, rating_master,
                                         'L' + str(int(max_level_master) - 1), 'L' + str(max_level_master)])
        else:
            print("without_gmc new")
            mail_list[mgr_email] = [[_id, master['Emp Name  First Name'].values.tolist()[0] + ' ' +
                                     master['Emp Name  Last Name'].values.tolist()[0],
                                    rating_new, rating_master,
                                     'L' + str(int(max_level_master) - 1), 'L' + str(max_level_master)]]


def with_gmc(master, new, _id, data3, mail_list):

    # cb stands for Compensation Bucketing
    master_without_cb = master[~master['Session Name'].str.contains("Compensation Bucketing : ")]
    new_without_cb = new[~new['Session Name'].str.contains("Compensation Bucketing : ")]

    max_level_master = master_without_cb['Session Name'].str.split(' ').str[2].str[1]
    max_level_new = new_without_cb['Session Name'].str.split(' ').str[2].str[1]

    # If max level for master and new is not equal than return
    if max_level_master.max() != max_level_new.max():
        return

    # Get max level value for master and new
    max_level_master = max_level_master.max()
    max_level_new = max_level_new.max()

    # Append level column in master and new for level
    master_without_cb['level'] = master_without_cb['Session Name'].str.split(' ').str[2].str[1]
    new_without_cb['level'] = new_without_cb['Session Name'].str.split(' ').str[2].str[1]

    # Check the status of max of master GMC level and max of new GMC level
    master_status = master[master['Session Name'].str.contains("Compensation Bucketing : ")]['Session Status'].values.tolist()[0]
    new_status = new[new['Session Name'].str.contains("Compensation Bucketing : ")]['Session Status'].values.tolist()[0]

    # If master_status is in progress and new_status if approved than check the ratings
    if master_status.lower() == 'In Progress'.lower() and new_status.lower() == 'Approved'.lower():

        # Fetch rating of "master - 1" and "new"
        rating_master = master_without_cb[master_without_cb['level'] ==
                                          str(max_level_master)]['New Value Label'].values.tolist()[0]
        rating_new = new[new['Session Name'].str.contains("Compensation Bucketing : ")]['New Value Label'].values.tolist()[0]

        if rating_master != rating_master or rating_new != rating_new:
            return

        # If rating of master and rating of new is equal than return
        if rating_master == rating_new:
            return

        # Search in hierarchy table for the user id and level
#         mgr_email = data3[(data3['levels'] == (int(max_level_master) - 1)) &
#                           (data3['UserId'] == str(_id))]['mgr_email'].values.tolist()[0]

        max_level_session_name = master_without_cb[master_without_cb['level'] ==
                                                   str(max_level_master)]['Session Name'].values.tolist()[0]
        max_level_session_mgr_id = max_level_session_name.split('_')[1]

        gmc_level_session_name = master[master['Session Name'].str.contains("Compensation Bucketing : ")]['Session Name'].values.tolist()[0]
        gmc_level_session_mgr_id = gmc_level_session_name.split('_')[1]

        if max_level_session_mgr_id == gmc_level_session_mgr_id:
            return

        mgr_email = data3[data3['mgr_id'] == max_level_session_mgr_id]['mgr_email'].values.tolist()[0]

        if mgr_email in mail_list:
            print("with_gmc append")
            mail_list[mgr_email].append([_id, master['Emp Name  First Name'].values.tolist()[0] + ' ' +
                                     master['Emp Name  Last Name'].values.tolist()[0],
                                    rating_new, rating_master, 'gmc', 'L' + str(max_level_master)])
        else:
            print("with_gmc new")
            mail_list[mgr_email] = [[_id, master['Emp Name  First Name'][0] + ' ' +
                                     master['Emp Name  Last Name'][0],
                                    rating_new, rating_master, 'gmc', 'L' + max_level_master]]


def email_updates_func(master_file, temporary_file, hierarchy_file):

    mail_list = dict()

    print("master:", master_file)
    print("temp:", temporary_file)

    master_file=pd.read_csv(master_file)
    temporary_file = pd.read_csv(temporary_file)
    hierarchy_file =pd.read_csv(hierarchy_file)

    data1 = master_file
    data2 = temporary_file
    data3 = hierarchy_file

    comp_list = ['Compensation Bucketing', 'compensation bucketing', 'Compensation bucketing', 'compensation Bucketing']
    data1 = data1[data1["Session Name"].str.contains("|".join(comp_list))]
    data2 = data2[data2["Session Name"].str.contains("|".join(comp_list))]

    unique_id = pd.unique(data1['Emp Name  User Name'])

    records_master = list()
    for i in unique_id:
        temp = data1[data1['Emp Name  User Name'] == i]
        records_master.append([temp])


    records_new = list()
    for i in unique_id:
        temp = data2[data2['Emp Name  User Name'] == i]
        records_new.append([temp])


    for idx, i in enumerate(unique_id):
        _id = i

        # Fetch the dataFrame
        master = records_master[idx][0]
        new = records_new[idx][0]

        if len(new[new['Session Name'].str.contains("Compensation Bucketing : ")]):
            # With_gmc
            with_gmc(master, new, _id, data3, mail_list)
        else:
            # Without GMC
            without_gmc(master, new, _id, data3, mail_list)

    for k in mail_list.keys():
        df = pd.DataFrame(columns=['Team Member ID', 'Team Member Name', 'Current rating', 'Old rating', 'Current level',
                                   'Old level'])
        for idx, item in enumerate(mail_list[k]):
            df.loc[idx] = item
        # yag.send(to=k, subject='test mail', contents=tabulate(df, tablefmt='orgtbl',
        #               headers=['Index', 'Team Member ID','Team Member Name','Current rating', 'Old rating', 'Current level',
        #                       'Old level']))\
        send_email(to=k, subject='test mail', email_data=tabulate(df, tablefmt='orgtbl',
                      headers=['Index', 'Team Member ID','Team Member Name','Current rating', 'Old rating', 'Current level',
                              'Old level']))

        print(tabulate(df, tablefmt='orgtbl',
                      headers=['Index', 'Team Member ID','Team Member Name','Current rating', 'Old rating', 'Current level',
                              'Old level']))


if __name__ == '__main__':
    # master_file = 'C:/Users/akala/Desktop/calibration/backend/session/files/EMAIL_MATCHING_MASTER/CalibrationFinal_2018_03_09_08_49_48.csv'
    # temporary_file = 'C:/Users/akala/Desktop/calibration/backend/session/files/EMAIL_MATCHING_TEMP/CalibrationFinal_2018_03_09_08_53_12.csv'
    # hierarchy_file = 'C:/Users/akala/Desktop/calibration/backend/session/files/hierarchy/hierarchy_file.csv'
    email_updates_func(master_file, temporary_file, hierarchy_file)
