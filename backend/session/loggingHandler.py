import logging
import os
import sys

class loggingHandler(logging.FileHandler):
    def __init__(self, file_name, mode, encoding, delay):
        super(loggingHandler, self).__init__(file_name, mode, encoding, delay)


    @staticmethod
    def configure_location(folder_path):
        if not os.path.exists(folder_path):
            try:
                os.makedirs(folder_path, exist_ok=True)
            except Exception as e:
                raise
