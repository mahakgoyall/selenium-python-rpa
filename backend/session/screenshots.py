from time import gmtime, strftime

def screenshots(browser, file_name):
    time_now = strftime("%H-%M-%S", gmtime())
    print(time_now)
    name = 'error_screenshots\\error_'+ str(file_name) + str(time_now) + '.png'
    print(name, type(name))
    browser.save_screenshot(name)
    return name
