from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
import os
import smtplib
from configparser import RawConfigParser
from logging.config import fileConfig
import configparser


def send_email(screenshot_name=None, email_data=None, to=None, subject=None):
    # configuration file setup
    config = RawConfigParser()
    config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

    msg = MIMEMultipart()
    msg['Subject'] = str(config['mail']['email_subject'])
    text = MIMEText(email_data)
    msg.attach(text)

    if screenshot_name:
        ImgFileName = screenshot_name
        with open(ImgFileName, 'rb') as f:
            img_data = f.read()
        image = MIMEImage(img_data, name=os.path.basename(ImgFileName))
        msg.attach(image)

    s = smtplib.SMTP(str(config['mail']['email_smtp']))
    if to:
        s.send_message(msg, from_addr=str(config['mail']['sender_mail']),
                       to_addrs=to)
    else:
        s.send_message(msg, from_addr=str(config['mail']['sender_mail']),
                       to_addrs=config['mail']['receiver_mail'].split(','))
