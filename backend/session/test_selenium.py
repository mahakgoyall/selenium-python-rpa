import sys
import urllib.parse
import sqlalchemy
import yagmail
import configparser
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from os import listdir
from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException, ElementNotVisibleException, WebDriverException
from selenium.webdriver.common.keys import Keys
import time
from bs4 import BeautifulSoup
import sys
import csv
import os
import logging
from logging.config import fileConfig
import csv
import re
import numpy as np
import pandas as pd
import pickle


def error_logging():
    fileConfig(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'logging_config.ini'))
    logger = logging.getLogger()
    return logger


# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

# yagmail setup
yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
yag = yagmail.SMTP(str(config['mail']['sender_mail']))


def connection_open():
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) +
                                "};SERVER=" + str(config['connection']['server']) + ";DATABASE=" +
                                str(config['connection']['database']) + ";UID=" + str(config['connection']['uid']) +
                                ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine


# def session_progress():
# 	engine = connection_open()
# 	db_logs = pd.read_sql_query("select * from Processing_Status", engine)
# 	sample_logs = pd.read_csv('CalibrationSample_2018_02_08_05_06_48.csv')
# 	sample_logs.rename(columns={"Session Name":"session_name", "Session Status":"session_status"}, inplace=True)
# 	merged_df = pd.merge(db_logs, sample_logs, on='session_name')
# 	merged_df.drop(merged_df.columns.difference(['session_name','flag','session_status']), 1, inplace=True)
# 	print('merged_df: ', merged_df)


def filter_setup(status, browser):
    # clear filter before setting it up.
    select_filter_var = None
    select_filter_var_count = 5
    while not select_filter_var and select_filter_var_count > 0:
        try:
            select_filter_var_count -= 1
            select_filter_var = browser.find_elements_by_tag_name('span')
            for span in select_filter_var:
                if span.text.lower().strip() == 'filter':
                    span.click()
                    break
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not select_filter_var and select_filter_var_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find the filter on manage calibration page')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)

    clear_filter = None
    clear_filter_count = 5
    while not clear_filter and clear_filter_count > 0:
        try:
            clear_filter_count -= 1
            clear_filter = browser.find_element_by_xpath('//button[text()="Clear"]')
            clear_filter.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not clear_filter and clear_filter_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find clear filter entity in filter pop up.')
        sys.exit(str(config['system']['exit_code']))

    # set filters by setup
    select_filter_var = None
    select_filter_var_count = 5
    while not select_filter_var and select_filter_var_count > 0:
        try:
            select_filter_var_count -= 1
            select_filter_var = browser.find_elements_by_tag_name('span')
            for span in select_filter_var:
                if span.text.lower().strip() == 'filter':
                    span.click()
                    break
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not select_filter_var and select_filter_var_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find filter on the manage calibration page.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)
    choose_options = None
    choose_options_count = 5
    while not choose_options and choose_options_count > 0:
        try:
            choose_options_count -= 1
            choose_options = browser.find_element_by_class_name('sfMultiSelectButtonIcon')
            choose_options.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not choose_options and choose_options_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find drop down element on filter pop up.')
        sys.exit(str(config['system']['exit_code']))

    find_setup = None
    find_setup_count = 5
    while not find_setup and find_setup_count > 0:
        try:
            find_setup_count -= 1
            find_setup = browser.find_element_by_xpath('//label[text()="' + str(status) + '"]')
            find_setup.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not find_setup and find_setup_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find setup entity on filter.')
        sys.exit(str(config['system']['exit_code']))

    apply_filter = None
    apply_filter_count = 5
    while not apply_filter and apply_filter_count > 0:
        try:
            apply_filter_count -= 1
            apply_filter = browser.find_element_by_xpath('//button[text()="Apply Filter"]')
            apply_filter.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not apply_filter_count and apply_filter_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find apply button on filter.')
        sys.exit(str(config['system']['exit_code']))


def main_func():
    logger = error_logging()
    engine = connection_open()
    logger.info("entered in main function.")

    try:
        browser = webdriver.Chrome(executable_path=str(config['system']['crome_driver_path']))
    except WebDriverException as e:
        logger.error(str(e))
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents=str(e))
        sys.exit(str(config['system']['exit_code']))  # Type valid exit code

    browser.get(str(config['system']['web_url']))
    browser.maximize_window()

    username_var = None
    username_find_count = 5
    logger.debug("Trying to find username section")
    while not username_var and username_find_count > 0:
        try:
            username_find_count -= 1
            username_var = browser.find_element_by_id('username')
            username_var.send_keys(str(config['system']['login_username']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not username_var and username_find_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find username field in login page')
        sys.exit(str(config['system']['exit_code']))

    password_var = None
    password_find_count = 5
    while not password_var and password_find_count > 0:
        try:
            password_find_count -= 1
            password_var = browser.find_element_by_id('password')
            password_var.send_keys(str(config['system']['login_password']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)

    if not password_var and password_find_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find password field in login page')
        sys.exit(str(config['system']['exit_code']))

    login_link = None
    login_link_count = 5
    while login_link_count > 0 and not login_link:
        try:
            login_link_count -= 1
            login_link = browser.find_element_by_name('login')
            login_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if login_link_count <= 0 and not login_link:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find login button link in login page')
        sys.exit(str(config['system']['exit_code']))

    home_click = None
    home_click_count = 5
    while home_click_count > 0 and not home_click:
        try:
            home_click_count -= 1
            home_click = browser.find_element_by_id('customHeaderModulePickerBtn-content')
            home_click.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if home_click_count <= 0 and not home_click:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find home button attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    admin_center = None
    admin_center_count = 5
    while not admin_center and admin_center_count > 0:
        try:
            admin_center_count -= 1
            admin_center = browser.find_element_by_link_text('Admin Centre')
            admin_center.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not admin_center and admin_center_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find admin center attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    create_calibration = None
    create_calibration_count = 5
    while not create_calibration and create_calibration_count > 0:
        try:
            create_calibration_count -= 1
            create_calibration = browser.find_element_by_id("40_")
            create_calibration.send_keys("Mass Create Calibration Sessions")
            create_calibration.send_keys(Keys.ARROW_DOWN)
            create_calibration.send_keys(Keys.ENTER)
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not create_calibration and create_calibration_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find create calibration entity on home page')
        sys.exit(str(config['system']['exit_code']))

    sendfile_link = None
    sendfile_link_count = 5
    while not sendfile_link and sendfile_link_count > 0:
        try:
            sendfile_link_count -= 1
            sendfile_link = browser.find_element_by_name('fileData1')
            sendfile_link.send_keys(str(config['system']['file_upload']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not create_calibration and create_calibration_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t  find sendfile link entity on home page')
        sys.exit(str(config['system']['exit_code']))

    validatefile_link = None
    validatefile_link_count = 5
    while not validatefile_link and validatefile_link_count > 0:
        try:
            validatefile_link_count -= 1
            validatefile_link = browser.find_element_by_xpath('//*[@value="Validate Import File"]')
            validatefile_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not validatefile_link and validatefile_link_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t validate the file link entity on home page')
        sys.exit(str(config['system']['exit_code']))

    cretesession_link_count = 5
    cretesession_link = None
    while not cretesession_link and cretesession_link_count > 0:
        try:
            cretesession_link_count -= 1
            cretesession_link = browser.find_element_by_xpath('//*[@value="Create Sessions"]')
            cretesession_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not cretesession_link and cretesession_link_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t validate the create session link entity on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.error("File added")

    adminc_link = None
    adminc_link_count = 5
    while not adminc_link and adminc_link_count > 0:
        try:
            adminc_link_count -= 1
            adminc_link = browser.find_element_by_class_name('link')
            adminc_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not adminc_link and adminc_link_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t validate admin link entity on home page')
        sys.exit(str(config['system']['exit_code']))

    manage_calibration = None
    manage_calibration_count = 5
    while not manage_calibration and manage_calibration_count > 0:
        try:
            manage_calibration_count -= 1
            manage_calibration = browser.find_element_by_id("40_")
            manage_calibration.send_keys("Manage Calibration Sessions")
            manage_calibration.send_keys(Keys.ARROW_DOWN)
            manage_calibration.send_keys(Keys.ENTER)
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not manage_calibration and manage_calibration_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t validate manage calibration entity on home page')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)
    filter_setup('Setup', browser)

    # check if table exists or not
    find_table_full = None
    find_table_full_count = 5
    while not find_table_full and find_table_full_count > 0:
        try:
            time.sleep(10)
            browser.refresh()
            find_table_full_count -= 1
            html = browser.page_source
            soup = BeautifulSoup(html)
            find_table_full = soup.findAll('table', attrs={'class': 'globalTable dataGrid'})
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not find_table_full and find_table_full_count <= 0:
        yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                 contents='Couldn\'t find table contents on manage calibration.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)

    table_var = True
    # table_count = 5
    while table_var:
        try:
            html = browser.page_source
            soup = BeautifulSoup(html)
            # table_count -= 1
            table = soup.findAll('table', attrs={'class': 'globalTable dataGrid'})
        except NoSuchElementException as e:
            time.sleep(5)
            browser.refresh()
        except NoSuchAttributeException as e:
            time.sleep(5)
            browser.refresh()
        except ElementNotVisibleException as e:
            time.sleep(5)
            browser.refresh()
        except Exception as e:
            time.sleep(5)
            browser.refresh()
        # if not table and table_count <= 0:
        # 	browser.save_screenshot('error_screenshot_table.png')
        # 	yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
        # 		contents='Couldn\'t find table contents on manage calibration.')
        # 	sys.exit(str(config['system']['exit_code']))
        try:
            table_data = []
            res = None
            for elem in table:
                tr_elem = elem.findAll("tr", attrs={'class': 'headerRow'})
                if len(tr_elem) == 0:
                    res = elem

            rows = res.findAll("tr")
            for row in rows:
                cells = row.findAll("td")
                cells = [ele.text.strip() for ele in cells]
                table_data.append([ele for ele in cells if ele])
            if not table_data:
                break

            for detail in table_data:
                if detail[2] == 'Setup':
                    session_elem = detail[0]
                    logger.error('session activating: ' + str(session_elem))
                    session_crop = session_elem.split('_')
                    session_id = session_crop[1]
                    con = engine.connect()
                    trans = con.begin()
                    rs = con.execute("insert into Processing_Status (Session_Owner_id, session_name) "
                                     "VALUES ('" + str(session_id) + "', '" + str(session_elem) + "')")
                    trans.commit()
                    time.sleep(5)
                    user_linkelem = None
                    user_linkelem_count = 5
                    while not user_linkelem and user_linkelem_count > 0:
                        try:
                            user_linkelem_count -= 1
                            user_linkelem = browser.find_element_by_link_text(session_elem)
                            user_linkelem.click()
                        except NoSuchElementException as e:
                            time.sleep(5)
                            browser.refresh()
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                            browser.refresh()
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                            browser.refresh()
                        except Exception as e:
                            time.sleep(5)
                            browser.refresh()
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not user_linkelem and user_linkelem_count <= 0:
                            yag.send(to=str(config['mail']['receiver_mail']),
                                     subject=str(config['mail']['email_subject']),
                                     contents='Couldn\'t find session link elememt in manage calibration page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass

                    validation_link = None
                    validation_link_count = 5
                    while not validation_link and validation_link_count > 0:
                        try:
                            validation_link_count -= 1
                            validation_link = browser.find_element_by_link_text('Validation')
                            validation_link.click()
                        except NoSuchElementException as e:
                            time.sleep(5)
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                        except Exception as e:
                            time.sleep(5)
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not validation_link and validation_link_count <= 0:
                            yag.send(to=str(config['mail']['receiver_mail']),
                                     subject=str(config['mail']['email_subject']),
                                     contents='Couldn\'t find validation link field in login page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass

                    time.sleep(2)
                    span_tag = None
                    span_tag_count = 5
                    while not span_tag and span_tag_count > 0:
                        try:
                            span_tag_count -= 1
                            span_tag = browser.find_elements_by_tag_name('span')
                            for s in span_tag:
                                if s.text.lower().strip() == 'activate':
                                    try:
                                        s.click()
                                        break
                                    except Exception as e:
                                        pass
                        except NoSuchElementException as e:
                            time.sleep(5)
                            browser.refresh()
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                            browser.refresh()
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                            browser.refresh()
                        except Exception as e:
                            time.sleep(5)
                            browser.refresh()
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not span_tag and span_tag_count <= 0:
                            yag.send(to=str(config['mail']['receiver_mail']),
                                     subject=str(config['mail']['email_subject']),
                                     contents='Couldn\'t find span tag in login page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass

                    validate_link = None
                    validate_link_count = 5
                    while not validate_link and validate_link_count > 0:
                        try:
                            validate_link_count -= 1
                            validate_link = browser.find_element_by_class_name("globalPrimaryButton")
                            validate_link.click()
                            logger.error("session activated")
                        except NoSuchElementException as e:
                            time.sleep(5)
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                        except Exception as e:
                            time.sleep(5)
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not validate_link and validate_link_count <= 0:
                            yag.send(to=str(config['mail']['receiver_mail']),
                                     subject=str(config['mail']['email_subject']),
                                     contents='Couldn\'t find validate link field in login page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass

                    con = engine.connect()
                    trans = con.begin()
                    rs = con.execute("update Processing_Status set flag = '1' where session_name = '"
                                     + str(session_elem) + "'")
                    trans.commit()
                    logger.error('session activated: ' + str(session_elem))
        except Exception as e:
            try:
                alert_popup = None
                alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                if alert_popup:
                    alert_popup.click()
                else:
                    yag.send(to=str(config['mail']['receiver_mail']), subject=str(config['mail']['email_subject']),
                             contents='error occured during execution of script.')
                    sys.exit(str(config['system']['exit_code']))
            except Exception as e:
                pass

    logger.error("Done!")


if __name__ == '__main__':
    main_func()