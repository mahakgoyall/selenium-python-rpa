from django.shortcuts import render
from django.http.response import JsonResponse
from django.http import HttpResponseRedirect
from django.utils import timezone
from datetime import datetime, timedelta
import pytz
from threading import Timer
from . import session_finalizing as sf
from . import session_activation as ts
from . import rollback_selenium as rb
from . import delete_session_code as dsc
import glob
import shutil
from time import gmtime, strftime
from configparser import RawConfigParser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import os
import configparser
from logging.config import fileConfig
from . import file_download as file_d
from . import db_config as db
import urllib.parse
import sqlalchemy

config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))
Format_Date = '%Y-%m-%d'
Format_Time = '%H:%M:%S'

ist = pytz.timezone('Asia/Kolkata')

# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))
# config.read('config.ini')

def connection_open():
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) + "};SERVER=" + str(
        config['connection']['server']) + ";DATABASE=" + str(config['connection']['database']) + ";UID=" + str(
        config['connection']['uid']) + ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine

## go button api: This API will only save the data in config.ini file for both activation and finalizing
## Note: Here  tab == 'session' means activation tab


def configure_folder(directory):
    if directory.split('/')[-1] == '*csv':
        directory = '/'.join(directory.split('/')[:-1])
    if not os.path.exists(directory):
        os.makedirs(directory)


##      This function is use to implement folder approach in session_activation file. Thos function will check whether folder
##      is there or not if not then make the missing folders and then search for file in folder and run script accordingly

def Folder(file_number):
    '''
    :param file_number:
    :return:
    '''
    print("FOLDER function called with file_number:", file_number)
    configure_folder(config['system']['file_upload_level1'])
    configure_folder(config['system']['file_upload_level1_archive'])
    configure_folder(config['system']['file_upload_level2'])
    configure_folder(config['system']['file_upload_level2_archive'])
    configure_folder(config['system']['file_upload_level3'])
    configure_folder(config['system']['file_upload_level3_archive'])
    configure_folder(config['system']['file_upload_level4'])
    configure_folder(config['system']['file_upload_level4_archive'])
    configure_folder(config['system']['file_upload_gmc'])
    configure_folder(config['system']['file_upload_gmc_archive'])
    print("FOLDER configured:", file_number)

    if file_number == 1:
        for file in glob.glob(str(config['system']['file_upload_level1'])):
            print("called ts_main")
            ts.main_func(file_number, file)
            # shutil.move(file, str(config['system']['file_upload_level1_archive']))
    elif file_number == 2:
        for file in glob.glob(str(config['system']['file_upload_level2'])):
            ts.main_func(file_number, file)
            # shutil.move(file, str(config['system']['file_upload_level2_archive']))
    elif file_number == 3:
        for file in glob.glob(str(config['system']['file_upload_level3'])):
            ts.main_func(file_number, file)
            # shutil.move(file, str(config['system']['file_upload_level3_archive']))
    elif file_number == 4:
        for file in glob.glob(str(config['system']['file_upload_level4'])):
            ts.main_func(file_number, file)
            # shutil.move(file, str(config['system']['file_upload_level4_archive']))
    elif file_number == 5:
        for file in glob.glob(str(config['system']['file_upload_gmc'])):
            ts.main_func(file_number, file)
            # shutil.move(file, str(config['system']['file_upload_gmc_archive']))


### Daily email update function that makes daily threads for email_updates

def daily_email_update():
    global GLOBAL_EMAIL_UPDATE_THREAD
    GLOBAL_EMAIL_UPDATE_THREAD.cancel()
    GLOBAL_EMAIL_UPDATE_THREAD = Timer(int(86400), daily_email_update, ())
    GLOBAL_EMAIL_UPDATE_THREAD.start()
    file_d.main_func()


## Email_update Function run for the firsttime only to check the time andd make a thread for daily_email_update function

def email_update():
    date_now = timezone.now().date()
    time_now = strftime("%H:%M:%S", gmtime())
    # TODO DONE
    # date_now = datetime.now(ist).date()
    # time_now = datetime.now(ist).strftime("%H:%M:%S")
    time_now = datetime.strptime(time_now, Format_Time).time()

    global GLOBAL_EMAIL_UPDATE_THREAD
    time_email = datetime.strptime(str(config['EMAIL_UPDATES']['email_time']), Format_Time).time()
    if time_now <= time_email:
        date_email = date_now + timedelta(days=1)
        date1 = date_email - date_now
        time1 = datetime.strptime(time_email.isoformat(),
                                  Format_Time) - datetime.strptime(
            time_now.isoformat(), Format_Time)
        GLOBAL_EMAIL_UPDATE_THREAD.cancel()
        GLOBAL_EMAIL_UPDATE_THREAD = Timer(int(time1.seconds), daily_email_update, ())
        GLOBAL_EMAIL_UPDATE_THREAD.start()
    else:
        time1 = datetime.strptime(time_now.isoformat(),
                                  Format_Time) - datetime.strptime(
            time_email.isoformat(), Format_Time)
        GLOBAL_EMAIL_UPDATE_THREAD.cancel()
        GLOBAL_EMAIL_UPDATE_THREAD = Timer(int(86400 - int(time1.seconds)), daily_email_update, ())
        GLOBAL_EMAIL_UPDATE_THREAD.start()


        # function will check about thread time to run session_activation file with config file and run accordingly


def thread_func_test(number):
    '''
    :param number:
    :return:
    '''
    print('from_thread_func_number_new thead')
    date_now = timezone.now().date().isoformat()
    time_now = strftime("%H:%M:%S", gmtime())
    # TODO DONE
    # date_now = datetime.now(ist).date().isoformat()
    # time_now = datetime.now(ist).strftime("%H:%M:%S")

    global GLOBAL_LEVEL1_thread
    global GLOBAL_LEVEL2_thread
    global GLOBAL_LEVEL3_thread
    global GLOBAL_LEVEL4_thread
    global GLOBAL_GMC_thread
    global Format_Time
    global Format_Date
    if number == 1:
        print(date_now)
        print(time_now)
        print(config['DATE&TIME_TEST']['level1_date'])
        print(config['DATE&TIME_TEST']['level1_time'])
        print(date_now == config['DATE&TIME_TEST']['level1_date'])
        print(time_now == config['DATE&TIME_TEST']['level1_time'])
        if date_now == config['DATE&TIME_TEST']['level1_date'] and time_now == config['DATE&TIME_TEST']['level1_time']:
            print(number)
            Folder(number)
        else:
            print(time_now, number)
            date1 = datetime.strptime(config['DATE&TIME_TEST']['level1_date'], Format_Date) - datetime.strptime(
                date_now, Format_Date)
            time1 = datetime.strptime(config['DATE&TIME_TEST']['level1_time'], Format_Time) - datetime.strptime(
                time_now, Format_Time)
            GLOBAL_LEVEL1_thread.cancel()
            GLOBAL_LEVEL1_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_test, args=[1])
            GLOBAL_LEVEL1_thread.start()
    elif number == 2:
        if date_now == config['DATE&TIME_TEST']['level2_date'] and time_now == config['DATE&TIME_TEST']['level2_time']:
            print(number)
            Folder(number)
        else:
            print(time_now, number)
            date1 = datetime.strptime(config['DATE&TIME_TEST']['level2_date'], Format_Date) - datetime.strptime(
                date_now, Format_Date)
            time1 = datetime.strptime(config['DATE&TIME_TEST']['level2_time'], Format_Time) - datetime.strptime(
                time_now, Format_Time)
            GLOBAL_LEVEL2_thread.cancel()
            GLOBAL_LEVEL2_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_test, args=[2])
            GLOBAL_LEVEL2_thread.start()
    elif number == 3:
        if date_now == config['DATE&TIME_TEST']['level3_date'] and time_now == config['DATE&TIME_TEST']['level3_time']:
            print(number)
            Folder(number)
        else:
            print(time_now, number)
            date1 = datetime.strptime(config['DATE&TIME_TEST']['level3_date'], Format_Date) - datetime.strptime(
                date_now, Format_Date)
            time1 = datetime.strptime(config['DATE&TIME_TEST']['level3_time'], Format_Time) - datetime.strptime(
                time_now, Format_Time)
            GLOBAL_LEVEL3_thread.cancel()
            GLOBAL_LEVEL3_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_test, args=[3])
            GLOBAL_LEVEL3_thread.start()
    elif number == 4:
        if date_now == config['DATE&TIME_TEST']['level4_date'] and time_now == config['DATE&TIME_TEST']['level4_time']:
            print(number)
            Folder(number)
        else:
            print(time_now, number)
            date1 = datetime.strptime(config['DATE&TIME_TEST']['level4_date'], Format_Date) - datetime.strptime(
                date_now, Format_Date)
            time1 = datetime.strptime(config['DATE&TIME_TEST']['level4_time'], Format_Time) - datetime.strptime(
                time_now, Format_Time)
            GLOBAL_LEVEL4_thread.cancel()
            GLOBAL_LEVEL4_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_test, args=[4])
            GLOBAL_LEVEL4_thread.start()
    elif number == 5:
        if date_now == config['DATE&TIME_TEST']['gmc_date'] and time_now == config['DATE&TIME_TEST']['gmc_time']:
            print(number)
            Folder(number)
        else:
            print(time_now, number)
            date1 = datetime.strptime(config['DATE&TIME_TEST']['gmc_date'], Format_Date) - datetime.strptime(date_now,
                                                                                                             Format_Date)
            time1 = datetime.strptime(config['DATE&TIME_TEST']['gmc_time'], Format_Time) - datetime.strptime(time_now,
                                                                                                             Format_Time)
            GLOBAL_GMC_thread.cancel()
            GLOBAL_GMC_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_test, args=[5])
            GLOBAL_GMC_thread.start()
    else:
        print('No such form number. exists', number)
        pass
    return


# function will check about thread time to run finalizing script with config file and run accordingly

def thread_func_finalizing(number):
    '''
    :param number:
    :return:
    '''
    print('from_thread_func_number_new thread')
    date_now = timezone.now().date().isoformat()
    time_now = strftime("%H:%M:%S", gmtime())
    # TODO DONE
    # date_now = datetime.now(ist).date().isoformat()
    # time_now = datetime.now(ist).strftime("%H:%M:%S")

    global GLOBAL_LEVEL1_finalizing_thread
    global GLOBAL_LEVEL2_finalizing_thread
    global GLOBAL_LEVEL3_finalizing_thread
    global GLOBAL_LEVEL4_finalizing_thread
    global GLOBAL_GMC_finalizing_thread
    global Format_Time
    global Format_Date
    if number == 1:
        print("In finalizing")
        print(date_now)
        print(time_now)
        print(config['DATE&TIME_FINALIZING']['level1_date'])
        print(config['DATE&TIME_FINALIZING']['level1_time'])
        if date_now == config['DATE&TIME_FINALIZING']['level1_date'] and time_now == config['DATE&TIME_FINALIZING'][
            'level1_time']:
            print(number)
            sf.main_func()
        else:
            print(time_now, number)
            date1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level1_date'], Format_Date) - datetime.strptime(
                date_now, Format_Date)
            time1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level1_time'], Format_Time) - datetime.strptime(
                time_now, Format_Time)
            GLOBAL_LEVEL1_finalizing_thread.cancel()
            GLOBAL_LEVEL1_finalizing_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_finalizing,
                                                    args=[1])
            GLOBAL_LEVEL1_finalizing_thread.start()
    elif number == 2:
        if date_now == config['DATE&TIME_FINALIZING']['level2_date'] and time_now == config['DATE&TIME_FINALIZING'][
            'level2_time']:
            print(number)
            email_update()
            sf.main_func()
        else:
            print(time_now, number)
            date1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level2_date'], Format_Date) - datetime.strptime(
                date_now, Format_Date)
            time1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level2_time'], Format_Time) - datetime.strptime(
                time_now, Format_Time)
            GLOBAL_LEVEL2_finalizing_thread.cancel()
            GLOBAL_LEVEL2_finalizing_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_finalizing,
                                                    args=[2])
            GLOBAL_LEVEL2_finalizing_thread.start()
    elif number == 3:
        if date_now == config['DATE&TIME_FINALIZING']['level3_date'] and time_now == config['DATE&TIME_FINALIZING'][
            'level3_time']:
            print(number)
            sf.main_func()
        else:
            print(time_now, number)
            date1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level3_date'], Format_Date) - datetime.strptime(
                date_now, Format_Date)
            time1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level3_time'], Format_Time) - datetime.strptime(
                time_now, Format_Time)
            GLOBAL_LEVEL3_finalizing_thread.cancel()
            GLOBAL_LEVEL3_finalizing_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_finalizing,
                                                    args=[3])
            GLOBAL_LEVEL3_finalizing_thread.start()
    elif number == 4:
        if date_now == config['DATE&TIME_FINALIZING']['level4_date'] and time_now == config['DATE&TIME_FINALIZING'][
            'level4_time']:
            print(number)
            sf.main_func()
        else:
            print(time_now, number)
            date1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level4_date'], Format_Date) - datetime.strptime(
                date_now, Format_Date)
            time1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level4_time'], Format_Time) - datetime.strptime(
                time_now, Format_Time)
            GLOBAL_LEVEL4_finalizing_thread.cancel()
            GLOBAL_LEVEL4_finalizing_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_finalizing,
                                                    args=[4])
            GLOBAL_LEVEL4_finalizing_thread.start()
    elif number == 5:
        if date_now == config['DATE&TIME_FINALIZING']['gmc_date'] and time_now == config['DATE&TIME_FINALIZING'][
            'gmc_time']:
            print(number)
            sf.main_func()
        else:
            print(time_now, number)
            date1 = datetime.strptime(config['DATE&TIME_FINALIZING']['gmc_date'], Format_Date) - datetime.strptime(
                date_now,
                Format_Date)
            time1 = datetime.strptime(config['DATE&TIME_FINALIZING']['gmc_time'], Format_Time) - datetime.strptime(
                time_now,
                Format_Time)
            GLOBAL_GMC_finalizing_thread.cancel()
            GLOBAL_GMC_finalizing_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_finalizing,
                                                 args=[5])
            GLOBAL_GMC_finalizing_thread.start()
    else:
        print('number such form number. exists', number)
        pass
    return


GLOBAL_LEVEL1_FLAG = 0
GLOBAL_LEVEL2_FLAG = 0
GLOBAL_LEVEL3_FLAG = 0
GLOBAL_LEVEL4_FLAG = 0
GLOBAL_GMC_FLAG = 0

GLOBAL_LEVEL1_thread = Timer(10, thread_func_test, args=[1])
GLOBAL_LEVEL2_thread = Timer(10, thread_func_test, args=[2])
GLOBAL_LEVEL3_thread = Timer(10, thread_func_test, args=[3])
GLOBAL_LEVEL4_thread = Timer(10, thread_func_test, args=[4])
GLOBAL_GMC_thread = Timer(10, thread_func_test, args=[5])

GLOBAL_LEVEL1_FLAG_FINALIZING = 0
GLOBAL_LEVEL2_FLAG_FINALIZING = 0
GLOBAL_LEVEL3_FLAG_FINALIZING = 0
GLOBAL_LEVEL4_FLAG_FINALIZING = 0
GLOBAL_GMC_FLAG_FINALIZING = 0

GLOBAL_LEVEL1_finalizing_thread = Timer(10, thread_func_finalizing, args=[1])
GLOBAL_LEVEL2_finalizing_thread = Timer(10, thread_func_finalizing, args=[2])
GLOBAL_LEVEL3_finalizing_thread = Timer(10, thread_func_finalizing, args=[3])
GLOBAL_LEVEL4_finalizing_thread = Timer(10, thread_func_finalizing, args=[4])
GLOBAL_GMC_finalizing_thread = Timer(10, thread_func_finalizing, args=[5])

## email update thread

GLOBAL_EMAIL_UPDATE_THREAD = Timer(10, email_update, ())


class GO(APIView):
    @staticmethod
    def post(request):

        global GLOBAL_LEVEL1_thread
        global GLOBAL_LEVEL2_thread
        global GLOBAL_LEVEL3_thread
        global GLOBAL_LEVEL4_thread
        global GLOBAL_GMC_thread
        global GLOBAL_LEVEL1_FLAG
        global GLOBAL_LEVEL2_FLAG
        global GLOBAL_LEVEL3_FLAG
        global GLOBAL_LEVEL4_FLAG
        global GLOBAL_GMC_FLAG
        global Format_Date
        global Format_Time
        global GLOBAL_LEVEL1_finalizing_thread
        global GLOBAL_LEVEL2_finalizing_thread
        global GLOBAL_LEVEL3_finalizing_thread
        global GLOBAL_LEVEL4_finalizing_thread
        global GLOBAL_GMC_finalizing_thread
        global GLOBAL_LEVEL1_FLAG_FINALIZING
        global GLOBAL_LEVEL2_FLAG_FINALIZING
        global GLOBAL_LEVEL3_FLAG_FINALIZING
        global GLOBAL_LEVEL3_FLAG_FINALIZING
        global GLOBAL_LEVEL4_FLAG_FINALIZING
        global GLOBAL_GMC_FLAG_FINALIZING

        print('go', request.data)
        try:
            date_year = str(request.data['date']['year'])
            date_month = str(request.data['date']['month'])
            date_day = str(request.data['date']['day'])
            time_hour = str(request.data['time']['hour'])
            time_minute = str(request.data['time']['minute'])
            time_second = str(request.data['time']['second'])
            level = str(request.data['level'])
            tab = str(request.data['tab'])
            print(tab, date_year)
        except:
            print('Please send proper parameters')
            return JsonResponse({'output': 'Oops'})

        date_day = date_day if len(date_day) == 2 else '0' + date_day
        date_month = date_month if len(date_month) == 2 else '0' + date_month
        time_hour = time_hour if len(time_hour) == 2 else '0' + time_hour
        time_minute = time_minute if len(time_minute) == 2 else '0' + time_minute
        time_second = time_second if len(time_second) == 2 else '0' + time_second

        date_level = str(date_year) + '-' + str(date_month) + '-' + str(date_day)
        time_level = str(time_hour) + ':' + str(time_minute) + ':' + str(time_second)
        print(date_level, time_level)
        if tab == 'session':
            if level == '1':
                config['DATE&TIME_TEST']['level1_date'] = date_level
                config['DATE&TIME_TEST']['level1_time'] = time_level
                GLOBAL_LEVEL1_FLAG = 1
                print(GLOBAL_LEVEL1_FLAG)

            elif level == '2':
                config['DATE&TIME_TEST']['level2_date'] = date_level
                config['DATE&TIME_TEST']['level2_time'] = time_level
                GLOBAL_LEVEL2_FLAG = 1

            elif level == '3':
                config['DATE&TIME_TEST']['level3_date'] = date_level
                config['DATE&TIME_TEST']['level3_time'] = time_level
                GLOBAL_LEVEL3_FLAG = 1

            elif level == '4':
                config['DATE&TIME_TEST']['level4_date'] = date_level
                config['DATE&TIME_TEST']['level4_time'] = time_level
                GLOBAL_LEVEL4_FLAG = 1

            elif level == '5':
                config['DATE&TIME_TEST']['gmc_date'] = date_level
                config['DATE&TIME_TEST']['gmc_time'] = time_level
                GLOBAL_GMC_FLAG = 1

        elif tab == 'finalizing':
            if level == '1':
                config['DATE&TIME_FINALIZING']['level1_date'] = date_level
                config['DATE&TIME_FINALIZING']['level1_time'] = time_level
                GLOBAL_LEVEL1_FLAG_FINALIZING = 1

            elif level == '2':
                config['DATE&TIME_FINALIZING']['level2_date'] = date_level
                config['DATE&TIME_FINALIZING']['level2_time'] = time_level
                GLOBAL_LEVEL2_FLAG_FINALIZING = 1
            elif level == '3':
                config['DATE&TIME_FINALIZING']['level3_date'] = date_level
                config['DATE&TIME_FINALIZING']['level3_time'] = time_level
                GLOBAL_LEVEL3_FLAG_FINALIZING = 1

            elif level == '4':
                config['DATE&TIME_FINALIZING']['level4_date'] = date_level
                config['DATE&TIME_FINALIZING']['level4_time'] = time_level
                GLOBAL_LEVEL4_FLAG_FINALIZING = 1

            elif level == '5':
                config['DATE&TIME_FINALIZING']['gmc_date'] = date_level
                config['DATE&TIME_FINALIZING']['gmc_time'] = time_level
                GLOBAL_GMC_FLAG_FINALIZING = 1
        else:
            return JsonResponse({'output': 'Oops'})

        with open((os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini')), 'w') as configfile:
            config.write(configfile)

        return JsonResponse({'output': 'ok'}, safe=False)

    @staticmethod
    def get(request):
        return Response()


## Submit API: This API is responsible for the creating thread. If flag == 1 then this API will start a thread for that function.
## It will work for both activation and finalizing

class Submit(APIView):
    @staticmethod
    def post(request):
        global GLOBAL_LEVEL1_thread
        global GLOBAL_LEVEL2_thread
        global GLOBAL_LEVEL3_thread
        global GLOBAL_LEVEL4_thread
        global GLOBAL_GMC_thread
        global GLOBAL_LEVEL1_FLAG
        global GLOBAL_LEVEL2_FLAG
        global GLOBAL_LEVEL3_FLAG
        global GLOBAL_LEVEL4_FLAG
        global GLOBAL_GMC_FLAG
        global Format_Date
        global Format_Time
        global GLOBAL_LEVEL1_finalizing_thread
        global GLOBAL_LEVEL2_finalizing_thread
        global GLOBAL_LEVEL3_finalizing_thread
        global GLOBAL_LEVEL4_finalizing_thread
        global GLOBAL_GMC_finalizing_thread
        global GLOBAL_LEVEL1_FLAG_FINALIZING
        global GLOBAL_LEVEL2_FLAG_FINALIZING
        global GLOBAL_LEVEL3_FLAG_FINALIZING
        global GLOBAL_LEVEL3_FLAG_FINALIZING
        global GLOBAL_LEVEL4_FLAG_FINALIZING
        global GLOBAL_GMC_FLAG_FINALIZING

        tab = str(request.data['tab'])
        print('tab: ', tab)
        date_now = timezone.now().date().isoformat()
        time_now = strftime("%H:%M:%S", gmtime())
        # TODO DONE
        # date_now = datetime.now(ist).date().isoformat()
        # time_now = datetime.now(ist).strftime("%H:%M:%S")
        print(date_now)
        print(time_now)
        print(GLOBAL_LEVEL1_FLAG_FINALIZING)

        if tab == 'session':
            print("Inside session")
            print(GLOBAL_LEVEL1_FLAG)
            if GLOBAL_LEVEL1_FLAG == 1:
                date1 = datetime.strptime(config['DATE&TIME_TEST']['level1_date'], Format_Date) - datetime.strptime(
                    date_now,
                    Format_Date)
                time1 = datetime.strptime(config['DATE&TIME_TEST']['level1_time'], Format_Time) - datetime.strptime(
                    time_now,
                    Format_Time)
                print("date 1 and time 1")
                print(date1)
                print(time1)
                GLOBAL_LEVEL1_thread.cancel()
                GLOBAL_LEVEL1_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_test, args=[1])
                GLOBAL_LEVEL1_thread.start()

            if GLOBAL_LEVEL2_FLAG == 1:
                date1 = datetime.strptime(config['DATE&TIME_TEST']['level2_date'], Format_Date) - datetime.strptime(
                    date_now,
                    Format_Date)
                time1 = datetime.strptime(config['DATE&TIME_TEST']['level2_time'], Format_Time) - datetime.strptime(
                    time_now,
                    Format_Time)
                GLOBAL_LEVEL2_thread.cancel()
                GLOBAL_LEVEL2_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_test, args=[2])
                GLOBAL_LEVEL2_thread.start()

            if GLOBAL_LEVEL3_FLAG == 1:
                date1 = datetime.strptime(config['DATE&TIME_TEST']['level3_date'], Format_Date) - datetime.strptime(
                    date_now,
                    Format_Date)
                time1 = datetime.strptime(config['DATE&TIME_TEST']['level3_time'], Format_Time) - datetime.strptime(
                    time_now,
                    Format_Time)
                GLOBAL_LEVEL3_thread.cancel()
                GLOBAL_LEVEL3_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_test, args=[3])
                GLOBAL_LEVEL3_thread.start()

            if GLOBAL_LEVEL4_FLAG == 1:
                date1 = datetime.strptime(config['DATE&TIME_TEST']['level4_date'], Format_Date) - datetime.strptime(
                    date_now,
                    Format_Date)
                time1 = datetime.strptime(config['DATE&TIME_TEST']['level4_time'], Format_Time) - datetime.strptime(
                    time_now,
                    Format_Time)
                GLOBAL_LEVEL4_thread.cancel()
                GLOBAL_LEVEL4_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_test, args=[4])
                GLOBAL_LEVEL4_thread.start()

            if GLOBAL_GMC_FLAG == 1:
                date1 = datetime.strptime(config['DATE&TIME_TEST']['gmc_date'], Format_Date) - datetime.strptime(
                    date_now,
                    Format_Date)
                time1 = datetime.strptime(config['DATE&TIME_TEST']['gmc_time'], Format_Time) - datetime.strptime(
                    time_now,
                    Format_Time)
                GLOBAL_GMC_thread.cancel()
                GLOBAL_GMC_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_test, args=[5])
                GLOBAL_GMC_thread.start()

        elif tab == 'finalizing':
            print("finalizing:", GLOBAL_LEVEL1_FLAG_FINALIZING)
            if GLOBAL_LEVEL1_FLAG_FINALIZING == 1:
                date1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level1_date'],
                                          Format_Date) - datetime.strptime(
                    date_now, Format_Date)
                time1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level1_time'],
                                          Format_Time) - datetime.strptime(
                    time_now, Format_Time)
                GLOBAL_LEVEL1_finalizing_thread.cancel()
                GLOBAL_LEVEL1_finalizing_thread = Timer(int(time1.seconds + (date1.days * 86400)),
                                                        thread_func_finalizing, args=[1])
                GLOBAL_LEVEL1_finalizing_thread.start()

            if GLOBAL_LEVEL2_FLAG_FINALIZING == 1:
                date1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level2_date'],
                                          Format_Date) - datetime.strptime(
                    date_now, Format_Date)
                time1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level2_time'],
                                          Format_Time) - datetime.strptime(
                    time_now, Format_Time)
                GLOBAL_LEVEL2_finalizing_thread.cancel()
                GLOBAL_LEVEL2_finalizing_thread = Timer(int(time1.seconds + (date1.days * 86400)),
                                                        thread_func_finalizing, args=[2])
                GLOBAL_LEVEL2_finalizing_thread.start()
                print('thread created',time1)

            if GLOBAL_LEVEL3_FLAG_FINALIZING == 1:
                date1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level3_date'],
                                          Format_Date) - datetime.strptime(
                    date_now, Format_Date)
                time1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level3_time'],
                                          Format_Time) - datetime.strptime(
                    time_now, Format_Time)
                GLOBAL_LEVEL3_finalizing_thread.cancel()
                GLOBAL_LEVEL3_finalizing_thread = Timer(int(time1.seconds + (date1.days * 86400)),
                                                        thread_func_finalizing, args=[3])
                GLOBAL_LEVEL3_finalizing_thread.start()

            if GLOBAL_LEVEL4_FLAG_FINALIZING == 1:
                date1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level4_date'],
                                          Format_Date) - datetime.strptime(
                    date_now, Format_Date)
                time1 = datetime.strptime(config['DATE&TIME_FINALIZING']['level4_time'],
                                          Format_Time) - datetime.strptime(
                    time_now, Format_Time)
                GLOBAL_LEVEL4_finalizing_thread.cancel()
                GLOBAL_LEVEL4_finalizing_thread = Timer(int(time1.seconds + (date1.days * 86400)),
                                                        thread_func_finalizing, args=[4])
                GLOBAL_LEVEL4_finalizing_thread.start()

            if GLOBAL_GMC_FLAG_FINALIZING == 1:
                date1 = datetime.strptime(config['DATE&TIME_FINALIZING']['gmc_date'], Format_Date) - datetime.strptime(
                    date_now, Format_Date)
                time1 = datetime.strptime(config['DATE&TIME_FINALIZING']['gmc_time'], Format_Time) - datetime.strptime(
                    time_now, Format_Time)
                GLOBAL_GMC_finalizing_thread.cancel()
                GLOBAL_GMC_finalizing_thread = Timer(int(time1.seconds + (date1.days * 86400)), thread_func_finalizing,
                                                     args=[5])
                GLOBAL_GMC_finalizing_thread.start()

        return JsonResponse({'output': 'ok'})

    @staticmethod
    def get(request):
        return Response()


## Rollback API: This API is will run rollback script

class Rollback(APIView):
    @staticmethod
    def post(request):
        print("Rollback api called")
        tab = request.data['tab']
        if tab == 'finalizing':
            sf.main_func()
        else:
            rb.main_func()
        return JsonResponse({'output': 'ok'})

    @staticmethod
    def get(request):
        return Response()


## This API will run delete scprit

class Delete(APIView):
    @staticmethod
    def post(request):
        print("Delete api called")
        dsc.main_func()
        return JsonResponse({'output': 'ok'})

    @staticmethod
    def get(request):
        return Response()


# This API will GET and POST Settings tab data

class Setting(APIView):
    @staticmethod
    def post(request):
        try:
            driver = str(request.data['driver'])
            server = str(request.data['server'])
            database = str(request.data['database'])
            uid = str(request.data['uid'])
            password = str(request.data['password'])
            sender_email = str(request.data['sender_email'])
            receiver_email = str(request.data['receiver_email'])
            email_password = str(request.data['email_password'])
            email_subject = str(request.data['email_subject'])
            web_url = str(request.data['web_url'])
            login_username = str(request.data['login_username'])
            login_password = str(request.data['login_password'])

        except:
            print('Please send proper parameters')
            return JsonResponse({'output': 'Oops'})

        config['connection']['driver'] = str(request.data['driver'])
        config['connection']['server'] = str(request.data['server'])
        config['connection']['database'] = request.data['database']
        config['connection']['uid'] = request.data['uid']
        config['connection']['password'] = request.data['password']
        config['mail']['sender_mail'] = request.data['sender_email']
        config['mail']['receiver_mail'] = request.data['receiver_email']
        config['mail']['email_password'] = request.data['email_password']
        config['mail']['email_subject'] = request.data['email_subject']
        config['system']['web_url'] = request.data['web_url']
        config['system']['login_username'] = request.data['login_username']
        config['system']['login_password'] = request.data['login_password']
        with open((os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini')), 'w') as configfile:
            config.write(configfile)
        with open('session/admin.py', 'w') as f:
            f.write('from django.contrib import admin\n')
        return JsonResponse({'output': 'ok'})

    @staticmethod
    def get(request):
        iniform = {'driver': str(config['connection']['driver']),
                   'server': str(config['connection']['server']),
                   'database': str(config['connection']['database']),
                   'uid': str(config['connection']['uid']),
                   'password': str(config['connection']['password']),
                   'sender_email': str(config['mail']['sender_mail']),
                   'receiver_email': str(config['mail']['receiver_mail']),
                   'email_password': str(config['mail']['email_password']),
                   'email_subject': str(config['mail']['email_subject']),
                   'web_url': str(config['system']['web_url']),
                   'login_username': str(config['system']['login_username']),
                   'login_password': str(config['system']['login_password'])}
        if config['connection']['flag'] == '0':
            print("This scrpit will only run for once")
            db.main_func()
            config['connection']['flag'] = '1'
            with open((os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini')), 'w') as configfile:
                config.write(configfile)
        return Response(iniform)


## This API will save email update time in config.ini file

class email_updates(APIView):
    @staticmethod
    def post(request):
        try:
            time_hour = str(request.data['time']['hour'])
            time_minute = str(request.data['time']['minute'])
            time_second = str(request.data['time']['second'])
        except:
            print('Please send proper parameters')
            return JsonResponse({'output': 'Oops'})
        email_time = time_hour + ':' + time_minute + ':' + time_second

        #check in db the flag value for email updates as email updates will run only after finalizing.
        engine = connection_open()
        con = engine.connect()
        trans = con.begin()
        rs = con.execute("select flag from Finalizing_Status where session_name = 'email_updates'").fetchall()
        trans.commit()
        if rs[0][0] == 0:
            return JsonResponse({'output': 'Couldn\' update Email settings.'})

        config['EMAIL_UPDATES']['email_time'] = email_time
        with open((os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini')), 'w') as configfile:
            config.write(configfile)
        email_update()
        return JsonResponse({'output': 'ok'})

    @staticmethod
    def get(request):
        email_time_here = str(config['EMAIL_UPDATES']['email_time']).split(':')
        # print (email_time_here)
        email_update = {'email': {'hour': int(email_time_here[0]), 'minute': int(email_time_here[1]),
                                  'second': int(email_time_here[2])}}
        return JsonResponse(email_update)
