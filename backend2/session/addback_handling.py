# coding: utf-8
import pandas as pd
from tabulate import tabulate
import sys
import csv
import os
import logging
from logging.config import fileConfig
import glob
import collections
import urllib.parse
import sqlalchemy
import yagmail
import time
import configparser
import shutil
from time import gmtime, strftime
import datetime
import pytz
# from .send_email import send_email


def error_logging():
    fileConfig(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'logging_config.ini'))
    logger = logging.getLogger()
    return logger


# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))


# yagmail setup
# yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
# yag = yagmail.SMTP(str(config['mail']['sender_mail']))
# mail_list = config['mail']['receiver_mail'].split(',')
# mail_list_tuple = tuple((e.strip() for e in mail_list))

def connection_open():
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) +
                                "};SERVER=" + str(config['connection']['server']) + ";DATABASE=" +
                                str(config['connection']['database']) + ";UID=" + str(config['connection']['uid']) +
                                ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine


ist = pytz.timezone('Asia/Kolkata')


def update_template_levels(level, delete_flag, subject, manager):
    logger = error_logging()
    try:
        engine = connection_open()
        level_file_count = 0
        logger.info("if exists(select 1 from level" + str(level) + "_tbl where [Session Owner] = '" +
                    str(manager) + "') begin select count(*) from level" + str(
            level) + "_tbl where [Session Owner] = '" +
                    str(manager) + "' end else begin select 0 end")
        level_file_count = pd.read_sql_query(
            "if exists(select 1 from level" + str(level) + "_tbl where [Session Owner] = '" +
            str(manager) + "') begin select count(*) from level" + str(level) + "_tbl where [Session Owner] = '" +
            str(manager) + "' end else begin select 0 end", engine)
        print(level_file_count, "level_file_count")
        count_list = level_file_count.values.tolist()
        logger.info("File subject count for template change: " + str(count_list[0][0]))
        con = engine.connect()
        trans = con.begin()
        if int(count_list[0][0]) >= 30:
            con.execute("update level" + str(level) +
                                           "_tbl set Template = 'Compensation Clustering - V2' where [Session Owner] = '" +
                                           str(manager) + "'")
        else:
            con.execute("update level" + str(level) +
                                           "_tbl set Template = 'Compensation Clustering - V1' where [Session Owner] = '" +
                                           str(manager) + "'")
        trans.commit()
    except Exception as e:
        logger.error("Exception in template update function: " + str(e))
        return


def create_session_files(level, delete_flag, uploaded_file):
    """
    :param level:
    :return:
    This function is used for two cases:
    1.Addback case (flag = 0): moves the session files to archive folder at particular level and then executes the procedures
                    for the further levels and dumps the sql data for particular session into csv files
    2.Delete case (flag = 1): moves the session files to archive folder at particular level then delete the subjects and managers
                    accordingly and dumps the sql data for particular session into csv files
    """
    level = int(level)
    logger = error_logging()
    logger.info("in create_session_files")
    date_now = datetime.datetime.now(ist).date().isoformat()
    time_now = datetime.datetime.now(ist).strftime("%H-%M-%S")

    process_df = pd.read_csv(str(uploaded_file))
    process_df = process_df.fillna('')
    required_index = list(process_df['Username'].dropna().index)
    process_df = process_df.loc[required_index, :]
    print(process_df.shape)
    # for index, row in process_df.iterrows():
    #     print (row['Username'], type(row['Username']))
    # del process_df['']
    # print(process_df.shape)
    matching_data = []
    # Ids in the records are int and varchar; handling them
    for index, row in process_df.iterrows():
        try:
            temp_data = {}
            if type(row['Username']) == str and row['Username'] != '':
                temp_data['subject'] = str(row['Username'])
            elif type(row['Username']) != str and row['Username'] != '':
                temp_data['subject'] = int(row['Username'])

            if type(row['Reporting Manager User Sys ID']) == str and row['Reporting Manager User Sys ID'] != '':
                temp_data['manager'] = str(row['Reporting Manager User Sys ID'])
            elif type(row['Reporting Manager User Sys ID']) != str and row['Reporting Manager User Sys ID'] != '':
                temp_data['manager'] = int(row['Reporting Manager User Sys ID'])
            if temp_data != {}:
                print(temp_data)
                matching_data.append(temp_data)
        except Exception as e:
            print("Exception occur in creating list: ", str(e))
    print(matching_data, "matching_data")
    # Logic for deleting the Subject when Remove file uploaded during the process
    if delete_flag:
        engine = connection_open()
        logger.info("processing delete cases")

        for record in matching_data:
            try:
                con = engine.connect()
                trans = con.begin()
                logger.info("update CSV_DATA_NEW set Eligibility = 'no' where Username in ('" +
                            str(record['subject']) + "')")
                con.execute("update CSV_DATA_NEW set Eligibility = 'no' where Username in ('" +
                            str(record['subject']) + "')")
                trans.commit()
            except Exception as e:
                logger.error("Exception occured while inserting data in csv_data_new: " + str(e))

        while level <= 4:
            for record in matching_data:
                try:
                    # Delete records from the level tables
                    con = engine.connect()
                    trans = con.begin()
                    logger.info("if exists(select 1 from level" + str(level) + "_tbl where Subject = '" + str(
                        record['subject']) + "') begin delete from level" + str(level) + "_tbl where Subject = '" + str(
                        record['subject']) + "' end")
                    con.execute("if exists(select 1 from level" + str(level) + "_tbl where Subject = '" + str(
                        record['subject']) + "') begin delete from level" + str(level) + "_tbl where Subject = '" + str(
                        record['subject']) + "' end")
                    trans.commit()
                    con = engine.connect()
                    trans = con.begin()
                    logger.info("select [Session Owner] from level" + str(level) + 
                        "_tbl where Subject in ('" + str(record['subject']) + "')")
                    session_manager_df = pd.read_sql_query("select [Session Owner] from level" + str(level) + 
                        "_tbl where Subject in ('" + str(record['subject']) + "')", engine)
                    session_manager = session_manager_df.values.tolist()
                    logger.info("session_manager" + ' '.join(session_manager))
                    if len(session_manager) == 0:
                        continue
                except Exception as e:
                    logger.error("Exception occured in deleting records from level files: " + str(e))

                # Read level tables and count the records for template change
                update_template_levels(level, delete_flag, record['subject'], session_manager)

            # moving current session files to archive
            logger.info("level for delete file: " + str(level))
            source = str(config['system']['file_upload_level' + str(level)]).split('/')
            source = '/'.join(source[:-1])
            logger.info("source for delete file:" + source)

            # select all the files of the session folder and moves to archive of particular one
            for file in os.listdir(str(source)):
                try:
                    file_source = str(source) + "/" + str(file)
                    logger.info("final source for delete file: " + file_source)
                    destination = str(str(config['system']['file_upload_level' + str(level) + "_archive"]))
                    file_destination = str(
                        destination + str(file.split('.')[0]) + "_" + str(date_now) + "_" + str(time_now)) + ".csv"
                    logger.info("final destination for delete file: " + file_destination)
                    shutil.move(file_source, file_destination)
                except Exception as e:
                    logger.error("Exception occured in moving the files for delete: " + str(e))
            destination = str(config['files']['destination'])
            level_data = pd.read_sql_query("select * from level" + str(level) + "_tbl order by [Session Name] desc", engine)
            level_data.to_csv(str(destination) + 'LEVEL' + str(level) + '/level' + str(level) + '.csv',
                              index=False)
            # Process next level
            level += 1
        

    else:
        logger.info("processing addback cases")
        logger.info("current level: " + str(level))
        try:
            logger.info('database configuration starts')
            # insert into CSV_DATA_NEW if record doesn't exist else set eligibility to yes
            col_list = process_df.columns.get_values()
            col_list_str = '('
            for col in col_list:
                col_list_str += '[' + str(col) + '],'
            col_list_final = col_list_str[:-1]
            col_list_final += ')'

            engine = connection_open()
            # process records from uploaded file
            for index, row in process_df.iterrows():
                try:
                    con = engine.connect()
                    trans = con.begin()
                    if type(row['Username']) == float:
                        row['Username'] = int(row['Username'])
                    resultset = con.execute("select count(*) from CSV_DATA_ADDBACK where [Username] = '" + str(
                        row['Username']) + "' and [Eligibility] = 'yes'").fetchall()
                    trans.commit()
                    print(row['Username'], resultset[0][0])
                    if resultset[0][0] > 0:
                        #remove data from matching_data if exists in CSV_DATA_NEW and bypass below insertion
                        for index, sub in enumerate(matching_data):
                            if sub['subject'] == row['Username']:
                                matching_data.pop(index)
                                break
                        continue
                except Exception as e:
                    print("exception: ", str(e))
                
                temp_str = '('
                for col in col_list:
                    if type(row[col]) == float:
                        row[col] = int(row[col])
                    row_updated = str(row[col]).replace("'", " ")
                    temp_str += "'" + str(row_updated) + "',"
                temp_str = temp_str[:-1]
                temp_str += '),('
                query = "insert into CSV_DATA_NEW " + col_list_final + " values " + str(temp_str[:-2])
                con = engine.connect()
                trans = con.begin()
                logger.info("if exists(select 1 from CSV_DATA_NEW where [Username] = '" + str(
                    row['Username']) + "') begin Update CSV_DATA_NEW set [Eligibility] = 'yes' where [Username] = '" + str(
                    row['Username']) + "' end else begin " + query + " end")
                con.execute("if exists(select 1 from CSV_DATA_NEW where [Username] = '" + str(
                    row['Username']) + "') begin Update CSV_DATA_NEW set [Eligibility] = 'yes' where [Username] = '" + str(
                    row['Username']) + "' end else begin " + query + " end")
                trans.commit()

                # To add data in temporary addback file to be used later
                con = engine.connect()
                trans = con.begin()
                query = "insert into CSV_DATA_ADDBACK " + col_list_final + " values " + str(temp_str[:-2])
                logger.info("if exists(select 1 from CSV_DATA_ADDBACK where [Username] = '" + str(
                    row['Username']) + "') begin Update CSV_DATA_ADDBACK set [Eligibility] = 'yes' where [Username] = '" + str(
                    row['Username']) + "' end else begin " + query + " end")
                con.execute("if exists(select 1 from CSV_DATA_ADDBACK where [Username] = '" + str(
                    row['Username']) + "') begin Update CSV_DATA_ADDBACK set [Eligibility] = 'yes' where [Username] = '" + str(
                    row['Username']) + "' end else begin " + query + " end")
                trans.commit()
            logger.info('Data pushed to csv data new and csv data addback table.')

            print(len(matching_data), "len(matching_data)")
            if len(matching_data) == 0:
                return
            # Level insertion by hierarchy logic
            con = engine.connect()
            trans = con.begin()
            logger.info("exec hierarchy_proc_addback;")
            con.execute("exec hierarchy_proc_addback;")
            trans.commit()

            # truncate hierarchy_addback table
            con = engine.connect()
            trans = con.begin()
            logger.info("truncate table hierarchy_addback")
            con.execute("truncate table hierarchy_addback")
            trans.commit()

            logger.info("creating hierarchy addback table from existing hierarchy table")
            for record in matching_data:
                try:
                    subject_hierarchy_df = pd.read_sql_query("select * from hierarchy_tbl where UserId = '" +
                                                          str(record['subject']) + "'", engine)
                    # new_hierarchy_data = collections.defaultdict(list)
                    # for index, hierarchy_data in subject_hierarchy_df.iterrows():
                    #     for column_name in subject_hierarchy_df.columns:
                    #         new_hierarchy_data[column_name].append(hierarchy_data[column_name])
                    # new_hierarchy_df = pd.DataFrame(new_hierarchy_data)
                    print(str(record['subject']), subject_hierarchy_df)
                    subject_hierarchy_df.to_sql('hierarchy_addback', con=engine, if_exists='append', index=False)
                except Exception as e:
                    print("Exception during hierarchy addback: ", str(e))
              
            level_3_flag = True  
            while level <= 4:
                # run DB procedure for file creation for addback records insertion
                if level in [1, 2]:
                    con = engine.connect()
                    trans = con.begin()
                    con.execute("exec level" + str(level) + "_proc_addback;")
                    trans.commit()
                    logger.info("level1_addback procedure executed")
                elif level_3_flag:
                    level_3_flag = False
                    con = engine.connect()
                    trans = con.begin()
                    con.execute("exec fetching_userid_L3_L4_addback;")
                    trans.commit()
                    logger.info("fetching_userid_L3_L4_addback procedure executed")

                # Move the files to archive and add new files
                logger.info("level for addback file: " + str(level))
                source = str(config['system']['file_upload_level' + str(level)]).split('/')
                source = '/'.join(source[:-1])
                logger.info("source for addback file: " + source)

                # select all the files of the session folder and moves to archive of particular one
                for file in os.listdir(str(source)):
                    file_source = str(source) + "/" + str(file)
                    logger.info("source file for addback file:" + file_source)
                    destination = str(str(config['system']['file_upload_level' + str(level) + "_archive"]))
                    file_destination = str(
                        destination + str(file.split('.')[0]) + "_" + str(date_now) + "_" + str(time_now)) + ".csv"
                    logger.info("destination file for addback file:" + file_destination)
                    shutil.move(file_source, file_destination)

                # read the sql data and push the dump as csv into particular session folder
                logger.info("select * from level" + str(level) + "_tbl order by [Session Name] desc")
                level_data = pd.read_sql_query("select * from level" + str(level) + "_tbl order by [Session Name] desc", engine)
                logger.info('files//LEVEL' + str(level) + '//level' + str(level) + '.csv')
                level_data.to_csv(str(config['files']['destination']) + 'LEVEL' + str(level) + '/level' + str(
                    level) + '.csv', index=False)
                logger.info(
                    'addback case data moved to files//LEVEL' + str(level) + '//level' + str(level) + '.csv')

                # Process next level data
                level += 1
                    
        except Exception as e:
            print("Exception during Addback: ", e)


def fileUlpoad_dbInsert(addback_file, delete_flag):
    logger = error_logging()
    engine = connection_open()
    print("delete_flag....", delete_flag)

    # Fetch current_level from Processing_Status table to recognize the level of addback file
    con = engine.connect()
    trans = con.begin()
    rs = con.execute("select max(current_level) from Processing_Status").fetchall()
    trans.commit()
    level_num = rs[0][0]
    logger.info("current Level number: " + str(level_num))
    # level_num = '5'

    # Call function to create level and GMC files accordingly
    try:
        create_session_files(int(level_num) + 1, int(delete_flag), addback_file)
    except Exception as e:
        print("exception occur in calling the function: ", e)

if __name__ == '__main__':
    # fileUlpoad_dbInsert()
    # create_session_files(2)
    fileUlpoad_dbInsert("C:/Users/SinghalA/Desktop/temp.csv", 0)
