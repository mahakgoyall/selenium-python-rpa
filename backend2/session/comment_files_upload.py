import sys, collections
import urllib.parse
import sqlalchemy
import yagmail
import configparser
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException, ElementNotVisibleException, \
    WebDriverException
from selenium.webdriver.common.keys import Keys
import time
from bs4 import BeautifulSoup
import csv
import os
import logging
from logging.config import fileConfig
import glob
import shutil
from datetime import datetime
from .email_updates import email_updates_func
from .send_email import send_email
from selenium.webdriver.chrome.options import Options
from .screenshots import screenshots


def error_logging():
    fileConfig(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'logging_config.ini'))
    logger = logging.getLogger()
    return logger


# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(
    os.path.dirname(__file__)), 'config.ini'))


# yagmail setup
# yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
# yag = yagmail.SMTP(str(config['mail']['sender_mail']))
# mail_list = config['mail']['receiver_mail'].split(',')
# mail_list_tuple = tuple((e.strip() for e in mail_list))


def connection_open():
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) +
                                "};SERVER=" + str(config['connection']['server']) + ";DATABASE=" +
                                str(config['connection']['database']) + ";UID=" + str(config['connection']['uid']) +
                                ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine(
        "mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine


def main_func(comment_flag_file, comment_file):
    logger = error_logging()
    # engine = connection_open()
    logger.info("entered in main function of report upload for comment files")

    options = Options()
    options.set_headless(headless=True)
    try:
        # browser = webdriver.Chrome(executable_path=str(config['system']['crome_driver_path']))
        # browser = webdriver.Chrome()
        browser = webdriver.Chrome(executable_path=str(config['system']['crome_driver_path']), chrome_options=options)
    except WebDriverException as e:
        logger.error(str(e))
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=str(e))
        send_email(email_data=str(e))
        sys.exit(str(config['system']['exit_code']))  # Type valid exit code

    browser.get(str(config['system']['web_url']))
    browser.maximize_window()

    username_var = None
    username_find_count = 5
    logger.debug("Trying to find username section")
    while username_find_count > 0 and not username_var:
        try:
            username_find_count -= 1
            username_var = browser.find_element_by_id('username')
            username_var.send_keys(str(config['system']['login_username']))
            # username_var.send_keys('calib.admin')
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not username_var and username_find_count <= 0:
        logger.error('Couldn\'t find username field in login page')
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # browser.save_screenshot('error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find username field in login page')
        send_email(screenshot_name, 'Couldn\'t find username field in login page')
        sys.exit(str(config['system']['exit_code']))

    password_var = None
    password_find_count = 5
    while password_find_count > 0 and not password_var:
        try:
            password_find_count -= 1
            password_var = browser.find_element_by_id('password')
            password_var.send_keys(str(config['system']['login_password']))
            # password_var.send_keys('Test@1234')
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not password_var and password_find_count <= 0:
        logger.error('Couldn\'t find password field in login page')
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # browser.save_screenshot('error_download_foremail1.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find password field in login page')
        send_email(screenshot_name, 'Couldn\'t find password field in login page')
        sys.exit(str(config['system']['exit_code']))

    login_link_count = 5
    login_link = None
    while login_link_count > 0 and not login_link:
        try:
            login_link_count -= 1
            login_link = browser.find_element_by_name('login')
            login_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if login_link_count <= 0 and not login_link:
        logger.error('Couldn\'t find login button link in login page')
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # browser.save_screenshot('error_download_foremail2.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find login button link in login page')
        send_email(screenshot_name, 'Couldn\'t find login button link in login page')
        sys.exit(str(config['system']['exit_code']))

    home_click_count = 5
    home_click = None
    while home_click_count > 0 and not home_click:
        try:
            home_click_count -= 1
            time.sleep(3)
            home_click = browser.find_element_by_id('customHeaderModulePickerBtn-content')
            home_click.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if home_click_count <= 0 and not home_click:
        logger.error('Couldn\'t find home button attribute on home page')
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # browser.save_screenshot('error_download_foremail3.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find home button attribute on home page')
        send_email(screenshot_name, 'Couldn\'t find home button attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on Admin Center entity.")
    admin_center = None
    admin_center_count = 5
    while not admin_center and admin_center_count > 0:
        try:
            admin_center_count -= 1
            time.sleep(3)
            admin_center = browser.find_element_by_link_text('Admin Centre')
            admin_center.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not admin_center and admin_center_count <= 0:
        logger.error("Couldn\'t find Admin center entity.")
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find admin center attribute on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find admin center attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Enter 'Import Extended User Information' in search box entity.")
    create_calibration = None
    create_calibration_count = 5
    while not create_calibration and create_calibration_count > 0:
        try:
            create_calibration_count -= 1
            create_calibration = browser.find_element_by_id("40_")
            time.sleep(2)
            create_calibration.send_keys("Import Extended User Information")
            time.sleep(2)
            create_calibration.send_keys(Keys.ARROW_DOWN)
            create_calibration.send_keys(Keys.ENTER)
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not create_calibration and create_calibration_count <= 0:
        logger.error("Couldn\'t enter 'Import Extended User Information' in search box entity.")
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find create calibration entity on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find search box on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Uploading comment_flag_file")
    sendfile_link = None
    sendfile_link_count = 5
    while not sendfile_link and sendfile_link_count > 0:
        try:
            sendfile_link_count -= 1
            sendfile_link = browser.find_element_by_name('fbilpd_pm_file')
            print(sendfile_link)
            time.sleep(5)
            sendfile_link.send_keys(str(comment_flag_file))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not sendfile_link and sendfile_link_count <= 0:
        logger.error("Couldn\'t find file upload 'fbilpd_pm_file' link for comment flag file upload.")
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find create calibration entity on home page', '', screenshot_name])
        send_email(screenshot_name, "Couldn\'t find file upload 'fbilpd_pm_file' link for comment flag file upload")
        sys.exit(str(config['system']['exit_code']))

    logger.info("clicking on button to submit comment flag file")
    import_extended_file_button = None
    click_button_count = 5
    while not import_extended_file_button and click_button_count > 0:
        try:
            click_button_count -= 1
            import_extended_file_button = browser.find_element_by_name('fbilpd_pm_import_btn')
            time.sleep(2)
            import_extended_file_button.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not import_extended_file_button and click_button_count <= 0:
        logger.error("Couldn\'t find button for clicking 'Import Extended User Data File' ")
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find create calibration entity on home page', '', screenshot_name])
        send_email(screenshot_name,
                   "Couldn\'t find 'fbilpd_pm_import_btn' button for submitting comment upload report")
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on Admin Center entity again to upload comments file")
    admin_center = None
    admin_center_count = 5
    while not admin_center and admin_center_count > 0:
        try:
            admin_center_count -= 1
            admin_center = browser.find_element_by_link_text('Admin Centre')
            admin_center.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not admin_center and admin_center_count <= 0:
        logger.error("Couldn\'t find Admin center entity.")
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find admin center attribute on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find admin center attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Enter 'Import Extended User Information' in search box entity.")
    create_calibration = None
    create_calibration_count = 5
    while not create_calibration and create_calibration_count > 0:
        try:
            create_calibration_count -= 1
            create_calibration = browser.find_element_by_id("40_")
            time.sleep(2)
            create_calibration.send_keys("Import Extended User Information")
            time.sleep(2)
            create_calibration.send_keys(Keys.ARROW_DOWN)
            create_calibration.send_keys(Keys.ENTER)
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not create_calibration and create_calibration_count <= 0:
        logger.error("Couldn\'t enter 'Import Extended User Information' in search box entity.")
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find create calibration entity on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find search box on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Uploading comments file")
    sendfile_link = None
    sendfile_link_count = 5
    while not sendfile_link and sendfile_link_count > 0:
        try:
            sendfile_link_count -= 1
            sendfile_link = browser.find_element_by_name('fbilpd_pm_file')
            print(sendfile_link)
            time.sleep(5)
            sendfile_link.send_keys(str(comment_file))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not sendfile_link and sendfile_link_count <= 0:
        logger.error("Couldn\'t find file upload 'fbilpd_pm_file' link for comment flag file upload.")
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find create calibration entity on home page', '', screenshot_name])
        send_email(screenshot_name, "Couldn\'t find file upload 'fbilpd_pm_file' link for comment flag file upload")
        sys.exit(str(config['system']['exit_code']))

    logger.info("selecting radio button to select background information")
    check_background_info_box = None
    background_info_count = 5
    while not check_background_info_box and background_info_count > 0:
        try:
            background_info_count -= 1
            check_background_info_box = browser.find_element_by_xpath('//*[@value="fbilpd_format_background"]')
            time.sleep(2)
            check_background_info_box.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not check_background_info_box and background_info_count <= 0:
        logger.error("Couldn\'t find check box for selecting 'Background Information' ")
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find create calibration entity on home page', '', screenshot_name])
        send_email(screenshot_name,
                   "Couldn\'t find 'fbilpd_format_background' radio box for selecting Background Information")
        sys.exit(str(config['system']['exit_code']))

    logger.info("selecting radio button for incrementally adding data")
    check_incremental_data_box = None
    incremental_data_box_count = 5
    while not check_incremental_data_box and incremental_data_box_count > 0:
        try:
            incremental_data_box_count -= 1
            check_incremental_data_box = browser.find_element_by_xpath('//*[@value="fbilpd_opt_incremental_data"]')
            time.sleep(2)
            check_incremental_data_box.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not check_background_info_box and background_info_count <= 0:
        logger.error("Couldn\'t find check box for selecting 'Import by incrementally adding data' ")
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find create calibration entity on home page', '', screenshot_name])
        send_email(screenshot_name,
                   "Couldn\'t find 'fbilpd_opt_incremental_data' radio box for selecting Import by incrementally adding data")
        sys.exit(str(config['system']['exit_code']))

    logger.info("clicking on button to submit file")
    import_extended_file_button = None
    click_button_count = 5
    while not import_extended_file_button and click_button_count > 0:
        try:
            click_button_count -= 1
            import_extended_file_button = browser.find_element_by_name('fbilpd_pm_import_btn')
            time.sleep(2)
            import_extended_file_button.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not import_extended_file_button and click_button_count <= 0:
        logger.error("Couldn\'t find button for clicking 'Import Extended User Data File' ")
        screenshot_name = screenshots(browser, 'comments_upload_error')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find create calibration entity on home page', '', screenshot_name])
        send_email(screenshot_name,
                   "Couldn\'t find 'fbilpd_pm_import_btn' button for submitting comment upload report")
        sys.exit(str(config['system']['exit_code']))

    logger.info("successfully completed file upload script")
    browser.quit()

if __name__ == '__main__':
    comment_flag_file = str()
    comment_file = str()
    main_func(comment_flag_file, comment_file)
    # upload_file('C:/Users/Celebal/Desktop/New folder (2)/PersonalInfoData_TataComm Comment Flag.csv', 'C:/Users/Celebal/Desktop/New folder (2)/Compansationbucket_comment.csv')
    # create_comment_files('Compensation_Clustering_Comments_2019_02_21_02_55_23.csv', 2)
