import pandas as pd
import sys
import urllib.parse
import sqlalchemy
import yagmail
import configparser
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from os import listdir
from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException, ElementNotVisibleException, \
    WebDriverException
from selenium.webdriver.common.keys import Keys
import time
from bs4 import BeautifulSoup
import sys
import csv
import os
import logging
from logging.config import fileConfig

def error_logging():
    fileConfig(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'logging_config.ini'))
    logger = logging.getLogger()
    return logger


# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))
# config.read('config.ini')

# yagmail setup
# yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
# yag = yagmail.SMTP(str(config['mail']['sender_mail']))


def connection_open():
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) + "};SERVER=" + str(
        config['connection']['server']) + ";DATABASE=" + str(config['connection']['database']) + ";UID=" + str(
        config['connection']['uid']) + ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine

def main_func():
    try:
        logger = error_logging()
        logger.info('database configuration starts')
        # return
        engine = connection_open()
        # con = engine.connect()
        # trans = con.begin()
        # con.execute("trucate table CSV_DATA_NEW")
        # trans.commit()
        # raw_file = pd.read_csv(str(config['files']['path_to_dataFile']))
        # raw_file_na = raw_file.fillna('')
        # col_list = raw_file_na.columns.get_values()
        # col_list_str = '('
        # for col in col_list:
        #     col_list_str += '[' + str(col) + '],'
        # col_list_final = col_list_str[:-1]
        # col_list_final += ')'
        # print(col_list_final)
        # temp_str = '('
        # for index, row in raw_file_na.iterrows():
        #     for col in col_list:
        #         temp_str += "'" + str(row[col]) + "',"
        #     temp_str = temp_str[:-1]
        #     temp_str += '),'
        #     print(temp_str)
        #     bulk_query = "insert into CSV_DATA_NEW " + col_list_final + " values " + str(temp_str[:-1])
        # con = engine.connect()
        # trans = con.begin()
        # con.execute(bulk_query)
        # trans.commit()
        # with engine.connect() as conn, conn.begin() as trans:
        #     raw_file_na.to_sql('CSV_DATA_NEW', conn, if_exists='replace', index=False)
        # print('raw data pushed to db table.')

        destination_path = str(config['files']['destination'])
        #run PMP_Eligibility for PMP cases
        # con = engine.connect()
        # trans = con.begin()
        # logger.info('database PMP Eligibility procedure starts')
        # rs = con.execute("exec pmp_Eligibility;")
        # trans.commit()

        # #store heirarchy file
        # con = engine.connect()
        # trans = con.begin()
        logger.info('database hierarchy procedure starts')
        # rs = con.execute("exec hierarchy_proc;")
        # trans.commit()
        # hierarchy_data = pd.read_sql_query("select * from hierarchy_tbl", engine)
        # hierarchy_data.to_csv(str(destination_path) + 'hierarchy//hierarchy_file.csv', index=False)
        #store level1 file
        # con = engine.connect()
        # trans = con.begin()
        # logger.info('database level 1 starts')
        # rs = con.execute("exec level1_proc;")
        # trans.commit()
        # level1_data = pd.read_sql_query("select * from level1_tbl order by [Session Name] desc", engine)
        # level1_data.to_csv(str(destination_path) + 'LEVEL1//level1.csv', index=False)
        #store level2 files
        # con = engine.connect()
        # trans = con.begin()
        # logger.info('database level 2 starts')
        # rs = con.execute("exec level2_proc;")
        # trans.commit()
        # level2_data = pd.read_sql_query("select * from level2_tbl order by [Session Name] desc", engine)
        # level2_data.to_csv(str(destination_path) + 'LEVEL2//level2.csv', index=False)
        #level3 and level4 formed by calling single procedure
        # con = engine.connect()
        # trans = con.begin()
        # logger.info('database level 3 and level 4 starts')
        # rs = con.execute("exec fetching_userid_L3_L4;")
        # trans.commit()
        # level3_data = pd.read_sql_query("select * from level3_tbl order by [Session Name] desc", engine)
        # level3_data.to_csv(str(destination_path) + 'LEVEL3//level3.csv', index=False)
        # level4_data = pd.read_sql_query("select * from level4_tbl order by [Session Name] desc", engine)
        # level4_data.to_csv(str(destination_path) + 'LEVEL4//level4.csv', index=False)
        #logic for GMC files
        # con = engine.connect()
        # trans = con.begin()
        # logger.info('database GMC func starts')
        # rs = con.execute("exec populating_GMC_info;")
        # trans.commit()
        # con = engine.connect()
        # trans = con.begin()
        # GMC_names = con.execute("select * from GMC_tables_created").fetchall()
        # for GMC_name in GMC_names:
        #     con = engine.connect()
        #     trans = con.begin()
        #     print('gmc names: ', GMC_name[0])
        #     GMC_table = pd.read_sql_query("select * from [" + str(GMC_name[0]) + "] order by [Session Name] desc", engine)
        #     GMC_updated = GMC_name[0].replace('/', '_')
        #     GMC_table.to_csv(str(destination_path) + 'GMC//'+str(GMC_updated)+'.csv', index=False)

        #truncate logging tables
        # con = engine.connect()
        # trans = con.begin()
        # rs = con.execute("truncate table Processing_Status; truncate table Finalizing_Status;")
        # trans.commit()

        # #insert the guidelines and email updates flag into the db table
        # con = engine.connect()
        # trans = con.begin()
        # rs = con.execute("insert into Finalizing_Status values ('null', 'guidelines', 'null'), ('null', 'email_updates', '0')")
        # trans.commit()

        # #insert dummy entry in Processing_status for addback/remove before L1 process
        con = engine.connect()
        # trans = con.begin()
        # rs = con.execute("insert into Processing_Status values ('null', 'addback/remove', '0', '0')")
        # trans.commit()

        # logger.info("level for file: " + str(file_number))
        # source = str(config['system']['file_upload_gmc']).split('/')
        # source = '/'.join(source[:-1])
        # logger.info("source for file:" + source)

        # select all the files of the session folder and moves to archive of particular one
        # for file in os.listdir(str(source)):
        #     file_source = str(source) + "/" + str(file)
        #     logger.info("final source for file: " + file_source)
        #     destination = str(str(config['system']['file_upload_gmc_archive']))
        #     file_destination = str(
        #         destination + str(file.split('.')[0]) + "_" + str(date_now) + "_" + str(time_now)) + ".csv"
        #     logger.info("final destination for file: " + file_destination)
        #     shutil.move(file_source, file_destination)

        # logic for GMC files

        # fetch all data of GMC files
        trans = con.begin()
        gmc_names = con.execute("select * from GMC_tables_created").fetchall()
        trans.commit()
        print(gmc_names)
        for gmc_name in gmc_names:
            con = engine.connect()
            trans = con.begin()
            print('gmc names: ', gmc_name[0])
            gmc_table = pd.read_sql_query("select * from " + str(gmc_name[0] + " order by [Session Name] desc"), engine)
            destination = str(config['files']['destination'])
            gmc_table.to_csv(destination + '/GMC/' + str(gmc_name[0]) + '.csv', index=False)
            trans.commit()
        # for file in glob.glob(str(config['system']['file_upload_gmc'])):
            # ts.main_func(file_number, file)
            # shutil.move(file, str(config['system']['file_upload_gmc_archive']))

        print('done')
    except Exceeption as e:
        logger.error(str(e))

        
if __name__ == '__main__':
    main_func()
