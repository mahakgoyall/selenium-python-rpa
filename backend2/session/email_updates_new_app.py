# coding: utf-8
import pandas as pd
import yagmail
from tabulate import tabulate
import sys
import csv
import os
import logging
from logging.config import fileConfig
import glob
import shutil
import urllib.parse
# import sqlalchemy
import time
import configparser
from .send_email import send_email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

pd.set_option('mode.chained_assignment', None)


def error_logging():
    fileConfig(os.path.join(os.path.abspath(
        os.path.dirname(__file__)), 'logging_config.ini'))
    logger = logging.getLogger()
    return logger


# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))


# yagmail setup
yag = yagmail.SMTP('testing.celebal@celebaltech.com', 'divaker@test@12', host='smtp.office365.com', port=587, smtp_ssl=False,
                                   smtp_starttls=True)
# mail_list = config['mail']['receiver_mail'].split(',')
# mail_list_tuple = tuple((e.strip() for e in mail_list))

# def connection_open():
#     params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) +
#                                 "};SERVER=" + str(config['connection']['server']) + ";DATABASE=" +
#                                 str(config['connection']['database']) + ";UID=" + str(config['connection']['uid']) +
#                                 ";pwd=" + str(config['connection']['password']))
#     engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
#     return engine


def email_body_formatter(df, manager_name, manager_cluster_change_stage):
    # text = """
    #                 Dear ////,
    #                 Compensation Clustering update – following employees cluster is updated at #### stage:
    #                 Team Members
    #                 {table}
    #                 For further details, please reach out to your manager or HR business partner. 

    #                 Regards,
    #                 HR Team

    #                 This is a system generated email, please do not reply."""
    html = """
                    <html><body><p>Dear ////,</p>
                    <p>Compensation Clustering update – following employees cluster is updated at #### stage:</p>
            
                    {table}
                    <p>For further details, please reach out to your manager or HR business partner. </p>
                    <p>Regards,<br>HR Team</p>
                    <p style= "font-style: italic;">This is a system generated email, please do not reply.</p>
                    </body></html>
                    """
    html = html.replace('////', str(manager_name))
    html = html.replace('####', str(manager_cluster_change_stage))
    required_column_list = ['Emp ID', 'Emp Name', 'Old Cluster', 'New Cluster', 'Updated by']
    html = html.format(table=df[required_column_list].to_html(index=False))
    html = html.replace('<html>',
                        '<html> \n <style>table, th, td {border: 1px solid black ! important;border-collapse: collapse ! important;text-align:left ! important;padding: 8px ! important;}</style>')
    # print(html)
    message = MIMEMultipart(
        "alternative", None, [MIMEText(html, 'html')])
    # message = MIMEMultipart("alternative")
    # print(html, text)
    # message
    return html


def check_cluster_change(master, user_id, hierarchy_data, mail_list, level):
    logger = error_logging()
    level = int(level)
    if level != 5:
        master_level_record = master[master["Session Name"].str.contains('L' + str(level))]
    else:
        master_level_record = master[master["Session Name"].str.contains('FY19 : ')]
    if master_level_record.shape[0]:
        master_level_manager_id = master_level_record['Owner User Name'].values.tolist()[0]
        master_level_manager_name = master_level_record['Owners'].values.tolist()[0]
        old_cluster = new_cluster = str()
        if master_level_record['Old Value Label'].values.tolist()[0] != '':
            old_cluster = master_level_record['Old Value Label'].values.tolist()[0].split('.')[1].strip()
        if master_level_record['New Value Label'].values.tolist()[0] != '':
            new_cluster = master_level_record['New Value Label'].values.tolist()[0].split('.')[1].strip()
        if old_cluster.lower() == new_cluster.lower():
            return

        gmc_session_names = ['FY19 : ', 'fY19 : ', 'Fy19 : ', 'fy19 : ']
        levels_not_required = ['L' + str(level), 'L' + str(level + 1), 'L' + str(level + 2), 'L' + str(level + 3)]
        sessions_not_required = levels_not_required + gmc_session_names

        below_level_master_records = master[~master["Session Name"].str.contains("|".join(sessions_not_required))]

        owner_ids = set(below_level_master_records['Owner User Name'])
        for manager_id in owner_ids:
            if manager_id == master_level_manager_id:
                continue

            mgr_email = hierarchy_data[hierarchy_data['mgr_id'] == manager_id]['mgr_email'].values.tolist()[0]
            updated_in_session = master_level_record['Session Name'].values[0]
            session_names = master[master['Owner User Name'] == manager_id]['Session Name'].values.tolist()
            # print(updated_in_session)
            if len(session_names) > 1:
                for session_name in session_names:
                    mgr_level = session_name.split(' ')[1]
                    if mgr_email in mail_list:
                        mail_list[mgr_email].append([user_id, master['Emp Name  First Name'].values.tolist()[0] + ' ' +
                                                     master['Emp Name  Last Name'].values.tolist()[0], old_cluster,
                                                     new_cluster, manager_id, master_level_manager_name, mgr_level])
                    else:
                        mail_list[mgr_email] = [[user_id, master['Emp Name  First Name'].values.tolist()[0] + ' ' +
                                                 master['Emp Name  Last Name'].values.tolist()[0], old_cluster,
                                                 new_cluster, manager_id, master_level_manager_name, mgr_level]]
            else:
                session_name = session_names[0]
                mgr_level = session_name.split(' ')[1]
                if mgr_email in mail_list:
                    mail_list[mgr_email].append([user_id, master['Emp Name  First Name'].values.tolist()[0] + ' ' +
                                                 master['Emp Name  Last Name'].values.tolist()[0], old_cluster,
                                                 new_cluster, manager_id, master_level_manager_name, mgr_level])
                else:
                    mail_list[mgr_email] = [[user_id, master['Emp Name  First Name'].values.tolist()[0] + ' ' +
                                             master['Emp Name  Last Name'].values.tolist()[0], old_cluster,
                                             new_cluster, manager_id, master_level_manager_name, mgr_level]]


def email_updates_func(audit_report, hierarchy_file):
    logger = error_logging()
    cluster_report = pd.read_csv(audit_report)
    hierarchy_file = pd.read_csv(hierarchy_file)

    cluster_data = cluster_report.fillna('')
    hierarchy_data = hierarchy_file.fillna('')

    comp_list = ['FY19', 'fY19', 'Fy19', 'fy19']
    cluster_data = cluster_data[cluster_data["Session Name"].str.contains("|".join(comp_list))]
    # print(cluster_data.shape)
    level_to_be_processed = 0
    in_progress_sessions = cluster_data[cluster_data['Session Status'] == 'In Progress']['Session Name'].values
    if len(in_progress_sessions):
        in_progress_sessions_level = [session.split(' ')[1] for session in in_progress_sessions]
        max_level = max(in_progress_sessions_level) if 'L' in max(in_progress_sessions_level) else 5
        if max_level != 5:
            max_level = int(max_level[1])
            print(max_level, "max level in file in case of in progress sessions")
            level_to_be_processed = max_level - 1
            print(level_to_be_processed, "level_to_be_processed")
            if level_to_be_processed == 1:
                return
        else:
            level_to_be_processed = max_level - 1
            print(level_to_be_processed, "level_to_be_processed")
    else:
        approved_sessions = cluster_data[cluster_data['Session Status'] == 'Approved']['Session Name'].values
        if len(approved_sessions):
            approved_sessions_level = [session.split(' ')[1] for session in approved_sessions]
            max_level = max(approved_sessions_level) if 'L' in max(approved_sessions_level) else 5
            if max_level != 5:
                max_level = int(max_level[1])
                print(max_level, "max level in file in case of approved sessions")
                level_to_be_processed = max_level
                print(level_to_be_processed, "level_to_be_processed")
                if level_to_be_processed == 1:
                    return
            else:
                level_to_be_processed = max_level
                print(level_to_be_processed, "level_to_be_processed")

    unique_id = pd.unique(cluster_data['Emp Name  User Name'])

    records_master = list()
    for i in unique_id:
        temp = cluster_data[cluster_data['Emp Name  User Name'] == i]
        records_master.append([temp])

    mail_list = dict()
    for index, user_id in enumerate(unique_id):
        master = records_master[index][0]
        check_cluster_change(master, user_id, hierarchy_data, mail_list, level_to_be_processed)
        # break
    # send_email(to='mahak.goyal@celebaltech.com', email_data=mail_list.keys(), flag=True)
    # pd.to_csv('data.csv', mail_list)
    # print(mail_list)
    count=0
    df1=pd.DataFrame()
    for mail_id in mail_list.keys():
        count += 1
        # if count <= 100:
        #     print(count, "in if")
        #     continue
        print(count, "out if")
        df = pd.DataFrame(
            columns=['Emp ID', 'Emp Name', 'Old Cluster', 'New Cluster', 'Session Owner', 'Updated by',
                     'Relationship'])
        for index, value in enumerate(mail_list[mail_id]):
            df.loc[index] = value

        manager_name_df = cluster_data[cluster_data['Owner User Name'] == df['Session Owner'].unique()[0]]['Owners']
        manager_name = manager_name_df.values[0]
        if ' ' in manager_name:
            manager_name_splitted = manager_name.split(' ')
            first_name = manager_name_splitted[0]
            last_name = manager_name_splitted[1]
            if len(first_name) > 3:
                manager_name = first_name
        else:
            manager_name = manager_name

        manager_cluster_change_stage = 'L' + str(level_to_be_processed) if int(level_to_be_processed) < 5 else 'GMC'
        df['Relationship'] = [relationship + ' Comp Manager' if 'gmc' not in relationship.lower() else relationship for
                              relationship in df['Relationship'].tolist()]

        # Sort df according to relation ship
        df = df.sort_values('Relationship')
        index = 0

        # Put gmc relationship on right order
        for index in range(df.shape[0]):
            if 'gmc' not in df.Relationship[index].lower():
                break
        if index != 0:
            df = pd.concat([df.iloc[index:, :], df.iloc[:index, :]])
        # required_column_list = ['Emp ID', 'Emp Name', 'Old Cluster', 'New Cluster', 'Updated by']
        # cluster_change_tabular_data = tabulate(df[required_column_list], tablefmt='psql',
        #                                        headers=required_column_list, showindex=False)
        # email_body = 'Dear ' + str(
        #     manager_name) + '\n\nCompensation Clustering update – following employees cluster is updated at ' + str(
        #     manager_cluster_change_stage) + ' stage:\n\nTeam Members\n' + str(cluster_change_tabular_data) + \
        #              '\nFor further details, please reach out to your manager or HR business partner.\n\n ' + \
        #              'Regards\nHR Team '
        # print(email_body)
        df1 = df1.append(df)
        df1['Manager Email'] = mail_id
        message = email_body_formatter(df, manager_name, manager_cluster_change_stage)
        send_email(to=mail_id, email_data=message, flag=True)
        logger.info(mail_id)
        logger.info(str(df)+ str(manager_name) + str(manager_cluster_change_stage))
        print("1.. " + str(mail_id))
        # send_email(to='paramvir.bhatia@tatacommunications.com', email_data=message, flag=True)
        # send_email(to='mahak.goyal@celebaltech.com', email_data=message, flag=True)
        # send_email(to='ayush.garg@celebaltech.com', email_data=message, flag=True)
        # break
    df1.to_csv('data.csv', index=False)

if __name__ == '__main__':
    audit_report = 'C:/Users/SinghalA/Desktop/calibration/backend/session/files/email files/Calibration_Audit_2019_2019_03_23_13_53_21.csv'
    hierarchy_file = 'C:/Users/SinghalA/Desktop/calibration/backend/session/files/hierarchy/hierarchy_file.csv'
    email_updates_func(audit_report, hierarchy_file)
