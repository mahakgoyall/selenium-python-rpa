import sys
import urllib.parse
import sqlalchemy
import yagmail
import configparser
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException, ElementNotVisibleException, \
    WebDriverException
from selenium.webdriver.common.keys import Keys
import time
from bs4 import BeautifulSoup
import sys
import csv
import os
import logging
from logging.config import fileConfig
import glob
import shutil
from datetime import datetime
from .screenshots import screenshots
# from .email_updates_new_app import email_updates_func
from .send_email import send_email


def error_logging():
    fileConfig(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'logging_config.ini'))
    logger = logging.getLogger()
    return logger


# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

# yagmail setup
# yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
# yag = yagmail.SMTP(str(config['mail']['sender_mail']))
# mail_list = config['mail']['receiver_mail'].split(',')
# mail_list_tuple = tuple((e.strip() for e in mail_list))


def connection_open():
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) +
                                "};SERVER=" + str(config['connection']['server']) + ";DATABASE=" +
                                str(config['connection']['database']) + ";UID=" + str(config['connection']['uid']) +
                                ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine


def file_moving(source, destination, keyword):
    logger = error_logging()
    date_time = list()
    file_list = list()
    for f in glob.glob(str(source + '/*' + keyword + '*')):
        try:
            file_list.append(f)
            Temp = f.split('/')
            Temp = Temp[Temp.__len__() - 1].split('.')
            file = ''.join(Temp[:Temp.__len__() - 1])
            Temp = file.split('_')
            print(Temp)
            date_time.append(
                datetime(int(Temp[3]), int(Temp[4]), int(Temp[5]), int(Temp[6]), int(Temp[7]), int(Temp[8])))
            print(date_time)
        except Exception as e:
            logger.error('Error in file moving process.' + str(e))
    latest = max(date_time)
    index_date = date_time.index(latest)
    file = file_list[index_date]
    print(file)
    if not glob.glob(str(destination + '/EMAIL_MATCHING_MASTER/*' + keyword + '*')):
        logger.info("downloaded audit report moved to master folder")
        shutil.move(file, str(destination + '/EMAIL_MATCHING_MASTER'))
    return str(destination + '/EMAIL_MATCHING_MASTER/' + file)
    # else:
    #     print("moved")
    #     shutil.move(file, str(destination + '/EMAIL_MATCHING_TEMP'))


def file_dumping(destination):
    for master_file in glob.glob(str(destination + '/EMAIL_MATCHING_MASTER/*.csv')):
        # print('I am going from Master to Archive')
        shutil.move(master_file, str(destination + '/EMAIL_MATCHING_ARCHIVE'))

    # for temp_file in glob.glob(str(destination + '/EMAIL_MATCHING_TEMP/*.csv')):
    #     # print('I am going from Temp to Master')
    #     shutil.move(temp_file, str(destination + '/EMAIL_MATCHING_MASTER'))


def remove_reports(browser):
    logger = error_logging()
    select_report_count = 5
    select_report = None
    while not select_report and select_report_count > 0:
        try:
            select_report_count -= 1
            select_report = browser.find_element_by_xpath('//input[@title="Select All"]')
            select_report.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not select_report and select_report_count <= 0:
        logger.error('Couldn\'t find generate report button on pop-up.')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find generate report button on pop-up.')
        send_email(screenshot_name, 'Couldn\'t find select all check box')
        sys.exit(str(config['system']['exit_code']))

    remove_report_count = 5
    remove_report = None
    while not remove_report and remove_report_count > 0:
        try:
            remove_report_count -= 1
            remove_report = browser.find_element_by_xpath('//input[@value="Remove"]')
            remove_report.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not remove_report and remove_report_count <= 0:
        logger.error('Couldn\'t find remove button to remove all reports')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find generate report button on pop-up.')
        send_email(screenshot_name, 'Couldn\'t find remove button to remove all reports')
        sys.exit(str(config['system']['exit_code']))


def main_func(file_type):
    logger = error_logging()
    engine = connection_open()
    logger.info("entered in main function of report download for email updates.")

    try:
        browser = webdriver.Chrome(executable_path=str(config['system']['crome_driver_path']))
    except WebDriverException as e:
        logger.error(str(e))
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=str(e))
        send_email(email_data=str(e))
        sys.exit(str(config['system']['exit_code']))  # Type valid exit code

    browser.get(str(config['system']['web_url']))
    browser.maximize_window()

    username_var = None
    username_find_count = 5
    logger.debug("Trying to find username section")
    while username_find_count > 0 and not username_var:
        try:
            username_find_count -= 1
            username_var = browser.find_element_by_id('username')
            username_var.send_keys(str(config['system']['login_username']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not username_var and username_find_count <= 0:
        logger.error('Couldn\'t find username field in login page')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find username field in login page')
        send_email(screenshot_name, 'Couldn\'t find username field in login page')
        sys.exit(str(config['system']['exit_code']))

    password_var = None
    password_find_count = 5
    logger.info('started finding password box')
    while password_find_count > 0 and not password_var:
        try:
            password_find_count -= 1
            password_var = browser.find_element_by_id('password')
            password_var.send_keys(str(config['system']['login_password']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not password_var and password_find_count <= 0:
        logger.error('Couldn\'t find password field in login page')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find password field in login page')
        send_email(screenshot_name, 'Couldn\'t find password field in login page')
        sys.exit(str(config['system']['exit_code']))

    login_link_count = 5
    login_link = None
    logger.info('started finding login button')
    while login_link_count > 0 and not login_link:
        try:
            login_link_count -= 1
            login_link = browser.find_element_by_name('login')
            login_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if login_link_count <= 0 and not login_link:
        logger.error('Couldn\'t find login button link in login page')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find login button link in login page')
        send_email(screenshot_name, 'Couldn\'t find login button link in login page')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(5)
    home_click_count = 5
    home_click = None
    logger.info('started finding home button')
    while home_click_count > 0 and not home_click:
        try:
            home_click_count -= 1
            home_click = browser.find_element_by_id('customHeaderModulePickerBtn-content')
            home_click.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if home_click_count <= 0 and not home_click:
        logger.error('Couldn\'t find home button attribute on home page')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find home button attribute on home page')
        send_email(screenshot_name, 'Couldn\'t find home button attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    calibration_link_count = 5
    calibration_link = None
    logger.info('started finding Analytics button')
    while not calibration_link and calibration_link_count > 0:
        try:
            calibration_link_count -= 1
            calibration_link = browser.find_element_by_link_text('Analytics')
            calibration_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not calibration_link and calibration_link_count <= 0:
        logger.error('Couldn\'t find Analytics attribute in drop down list.')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find Analytics attribute in drop down list.')
        send_email(screenshot_name, 'Couldn\'t find Analytics attribute in drop down list.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(3)
    reporting_var_count = 5
    reporting_var = None
    logger.info('started finding reporting link on analytics page')
    while not reporting_var and reporting_var_count > 0:
        try:
            reporting_var_count -= 1
            reporting_var = browser.find_element_by_link_text('Reporting')
            time.sleep(5)
            reporting_var.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not calibration_link and calibration_link_count <= 0:
        logger.error('Couldn\'t find reporting link on Analytics page.')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find reporting link on Analytics page.')
        send_email(screenshot_name, 'Couldn\'t find reporting link on Analytics page.')
        sys.exit(str(config['system']['exit_code']))

    # remove previously downloaded reports.
    scheduled_var_count = 5
    scheduled_var = None
    logger.info('started finding scheduled reports link on reporting page.')
    while not scheduled_var and scheduled_var_count > 0:
        try:
            scheduled_var_count -= 1
            scheduled_var = browser.find_element_by_link_text('Scheduled Reports')
            print("scheduled reports...", scheduled_var)
            scheduled_var.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not scheduled_var and scheduled_var_count <= 0:
        logger.error('Couldn\'t find ad hoc reports link on reporting page.')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find ad hoc reports link on reporting page.')
        send_email(screenshot_name, 'Couldn\'t find scheduled reports link on reporting page.')
        sys.exit(str(config['system']['exit_code']))

    remove_flag_count = 5
    remove_flag = None
    while not remove_flag and remove_flag_count > 0:
        try:
            remove_flag_count -= 1
            remove_flag = browser.find_element_by_xpath('//td[text()="No report found."]')
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not remove_flag and remove_flag_count <= 0:
        pass
    if not remove_flag:
        remove_reports(browser)

    adhoc_var_count = 5
    adhoc_var = None
    logger.info('trying to find ad hoc reports link on reporting page.')
    while not adhoc_var and adhoc_var_count > 0:
        try:
            adhoc_var_count -= 1
            adhoc_var = browser.find_element_by_link_text('Ad Hoc Reports')
            adhoc_var.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not adhoc_var and adhoc_var_count <= 0:
        logger.error('Couldn\'t find ad hoc reports link on reporting page.')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find ad hoc reports link on reporting page.')
        send_email(screenshot_name, 'Couldn\'t find ad hoc reports link on reporting page.')
        sys.exit(str(config['system']['exit_code']))

    CalibrationSample_count = 5
    CalibrationSample = None
    logger.info('started finding ' + str(file_type) + ' link on reporting page.')
    while not CalibrationSample and CalibrationSample_count > 0:
        try:
            CalibrationSample_count -= 1
            time.sleep(2)
            CalibrationSample = browser.find_element_by_link_text(file_type)
            CalibrationSample.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not CalibrationSample and CalibrationSample_count <= 0:
        logger.info('Couldn\'t find' + str(file_type) + ' link on reporting page.')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find calibration sample report link on reporting page.')
        send_email(screenshot_name, 'Couldn\'t find' + str(file_type) + ' link on reporting page.')
        sys.exit(str(config['system']['exit_code']))

    runoffile_link_count = 5
    runoffile_link = None
    logger.info('started finding run offline checkbox on pop-up.')
    while not runoffile_link and runoffile_link_count > 0:
        try:
            runoffile_link_count -= 1
            runoffile_link = browser.find_element_by_xpath('//label[text()="Run Offline"]')
            runoffile_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not runoffile_link and runoffile_link_count <= 0:
        logger.error('Couldn\'t find run offline checkbox on pop-up.')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find run offline checkbox on pop-up.')
        send_email(screenshot_name, 'Couldn\'t find run offline checkbox on pop-up.')
        sys.exit(str(config['system']['exit_code']))

    generate_btn_count = 5
    generate_btn = None
    logger.info('started finding generate report button on pop-up.')
    while not generate_btn and generate_btn_count > 0:
        try:
            generate_btn_count -= 1
            generate_btn = browser.find_element_by_xpath('//button[text()="Generate Report"]')
            generate_btn.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not generate_btn and generate_btn_count <= 0:
        logger.error('Couldn\'t find generate report button on pop-up.')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find generate report button on pop-up.')
        send_email(screenshot_name, 'Couldn\'t find generate report button on pop-up.')
        sys.exit(str(config['system']['exit_code']))

    browser.refresh()
    browser.refresh()
    time.sleep(5)
    # download the files for matching
    scheduled_var_count = 5
    scheduled_var = None
    logger.info('started finding ad hoc reports link on reporting page')
    while not scheduled_var and scheduled_var_count > 0:
        try:
            scheduled_var_count -= 1
            scheduled_var = browser.find_element_by_link_text('Scheduled Reports')
            scheduled_var.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not scheduled_var and scheduled_var_count <= 0:
        logger.error('Couldn\'t find ad hoc reports link on reporting page')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find ad hoc reports link on reporting page.')
        send_email(screenshot_name, 'Couldn\'t find ad hoc reports link on reporting page.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(10)
    browser.refresh()

    file_download = None
    file_download_count = 1
    logger.info('started finding file download link on reporting page.')
    while not file_download and file_download_count > 0:
        try:
            time.sleep(5)
            browser.refresh()
            file_downloads = browser.find_elements_by_link_text('1 file')
            if file_downloads:
                for file in file_downloads:
                    try:
                        file_list = []
                        file_list = file
                        file_download_count -= 1
                        file_list.click()
                    except Exception as e:
                        pass
            if file_download_count <= 0:
                file_download = True
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not file_download and file_download_count <= 0:
        logger.error('Couldn\'t find file download link on reporting page.')
        screenshot_name = screenshots(browser, 'error_download_foremail.png')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents='Couldn\'t find file download link on reporting page.')
        send_email(screenshot_name, 'Couldn\'t find file download link on reporting page.')
        sys.exit(str(config['system']['exit_code']))
    time.sleep(50)
    # browser.quit()

    logger.error('file download is completed')
    logger.error('File movement from folders start.')
    source = str(config['files']['source'])
    destination = str(config['files']['destination'])
    # keyword = 'Calibration_Audit_2019'
    # keyword = 'CalibrationFinal'
    # keyword1 = 'CalibrationSample'
    # file_moving(source, destination, keyword)
    if file_type == 'Calibration_Audit_2019':
        # file_moving(source, destination, file_type)
        #
        # master_file_1 = glob.glob(str(destination + 'EMAIL_MATCHING_MASTER/*' + file_type + '*'))
        # master_file = master_file_1[0]
        # master_file = '/'.join(master_file.split("\\"))
        # temporary_file_1 = glob.glob(str(destination + 'EMAIL_MATCHING_TEMP/*' + file_type + '*'))
        # if len(temporary_file_1) > 0:
        #     print(temporary_file_1)
        #     temporary_file = temporary_file_1[0]
        #     temporary_file = '/'.join(temporary_file.split("\\"))
        #     heirarchy_file = destination + 'hierarchy/hierarchy_file.csv'
        #     email_updates_func(master_file, temporary_file, heirarchy_file)
        #     file_dumping(destination)

        # file_type = file_type.replace(' ', '_')
        date_time = list()
        file_list = list()
        for f in glob.glob(str(source + '/*' + file_type + '*')):
            try:
                print(f, "f")
                file_list.append(f)
                Temp = f.split('/')
                Temp = Temp[Temp.__len__() - 1].split('.')
                file = ''.join(Temp[:Temp.__len__() - 1])
                Temp = file.split('_')
                date_time.append(
                    datetime(int(Temp[3]), int(Temp[4]), int(Temp[5]), int(Temp[6]), int(Temp[7]), int(Temp[8])))
            except Exception as e:
                logger.error('Error in file moving process.' + str(e))
        latest = max(date_time)
        index_date = date_time.index(latest)
        file = file_list[index_date]
        file_name = (file.split('/')[3]).split('\\')[1]
        moved_file = shutil.move(file, str(destination + 'email files'))
    else:
        file_type = file_type.replace(' ', '_')
        date_time = list()
        file_list = list()
        for f in glob.glob(str(source + '/*' + file_type + '*')):
            try:
                print(f, "f")
                file_list.append(f)
                Temp = f.split('/')
                Temp = Temp[Temp.__len__() - 1].split('.')
                file = ''.join(Temp[:Temp.__len__() - 1])
                Temp = file.split('_')
                date_time.append(
                    datetime(int(Temp[3]), int(Temp[4]), int(Temp[5]), int(Temp[6]), int(Temp[7]), int(Temp[8])))
            except Exception as e:
                logger.error('Error in file moving process.' + str(e))
        latest = max(date_time)
        index_date = date_time.index(latest)
        file = file_list[index_date]
        file_name = (file.split('/')[3]).split('\\')[1]
        moved_file = shutil.move(file, str(destination + 'comments'))
    return moved_file

if __name__ == '__main__':
    main_func(file_type)
