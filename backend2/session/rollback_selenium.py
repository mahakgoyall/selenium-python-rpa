import sys
import urllib.parse
import sqlalchemy
import yagmail
import configparser
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from os import listdir
from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException, ElementNotVisibleException, \
    WebDriverException
from selenium.webdriver.common.keys import Keys
import time
from bs4 import BeautifulSoup
import sys
import csv
import os
import logging
from logging.config import fileConfig
from configparser import RawConfigParser
from .screenshots import screenshots
from .send_email import send_email
from selenium.webdriver.chrome.options import Options


def error_logging():
    fileConfig(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'logging_config.ini'))
    logger = logging.getLogger()
    return logger


# configuration file setup
config = configparser.RawConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

# yagmail setup
# yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
# yag = yagmail.SMTP(str(config['mail']['sender_mail']))
# mail_list = config['mail']['receiver_mail'].split(',')
# mail_list_tuple = tuple((e.strip() for e in mail_list))


def connection_open():
    logger = error_logging()
    logger.info("Tring to establish connection of Database.")
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) + "};SERVER=" + str(config['connection']['server']) + ";DATABASE=" + str(config['connection']['database']) + ";UID=" + str(config['connection']['uid']) +";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine


def filter_setup(status, browser):
    # clear filter before setting it up.
    logger = error_logging()
    logger.info("Trying to find filter entity.")
    select_filter_var = None
    select_filter_var_count = 5
    while not select_filter_var and select_filter_var_count > 0:
        try:
            select_filter_var_count -= 1
            select_filter_var = browser.find_elements_by_tag_name('span')
            for span in select_filter_var:
                if span.text.lower().strip() == 'filter(1)' or span.text.lower().strip() == 'filter':
                    span.click()
                    break
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not select_filter_var and select_filter_var_count <= 0:
        logger.error("Couldn\'t find filter entity in filter setup.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find the filter on manage calibration page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find the filter on manage calibration page')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)
    logger.info("Clicking on Clear button.")
    clear_filter = None
    clear_filter_count = 5
    while not clear_filter and clear_filter_count > 0:
        try:
            clear_filter_count -= 1
            clear_filter = browser.find_element_by_xpath('//button[text()="Clear"]')
            clear_filter.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not clear_filter and clear_filter_count <= 0:
        logger.error("Couldn\'t find Clear button in filter setup.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find clear filter entity in filter pop up.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find clear filter entity in filter pop up.')
        sys.exit(str(config['system']['exit_code']))

    # set filters by setup
    logger.info("Trying to find Filter entity.")
    select_filter_var = None
    select_filter_var_count = 5
    while not select_filter_var and select_filter_var_count > 0:
        try:
            select_filter_var_count -= 1
            select_filter_var = browser.find_elements_by_tag_name('span')
            for span in select_filter_var:
                if span.text.lower().strip() == 'filter(1)' or span.text.lower().strip() == 'filter':
                    span.click()
                    break
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not select_filter_var and select_filter_var_count <= 0:
        logger.error("Couldn\'t find Filter entity in filter setup.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find filter on the manage calibration page.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find filter on the manage calibration page.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)
    logger.info("Trying to find drop-down attribute in filter.")
    choose_options = None
    choose_options_count = 5
    while not choose_options and choose_options_count > 0:
        try:
            choose_options_count -= 1
            choose_options = browser.find_element_by_class_name('sfMultiSelectButtonIcon')
            choose_options.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not choose_options and choose_options_count <= 0:
        logger.error("Couldn\'t find drop-down attribute in filter setup.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find drop down element on filter pop up.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find drop down element on filter pop up.')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Trying to set filter entity with appropriate status.")
    find_setup = None
    find_setup_count = 5
    while not find_setup and find_setup_count > 0:
        try:
            find_setup_count -= 1
            find_setup = browser.find_element_by_xpath('//label[text()="' + str(status) + '"]')
            find_setup.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not find_setup and find_setup_count <= 0:
        logger.error("Couldn\'tset filter entity with appropriate status in filter setup.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find setup entity on filter.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find setup entity on filter.')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on 'Apply Filter' button.")
    apply_filter = None
    apply_filter_count = 5
    while not apply_filter and apply_filter_count > 0:
        try:
            apply_filter_count -= 1
            apply_filter = browser.find_element_by_xpath('//button[text()="Apply Filter"]')
            apply_filter.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not apply_filter_count and apply_filter_count <= 0:
        logger.error("Couldn\'t set filter link 'Apply Filter in filter setup.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find apply button on filter.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find apply button on filter.')
        sys.exit(str(config['system']['exit_code']))


def main_func():
    logger = error_logging()
    # engine = connection_open()
    logger.info('Inside main function of session activation rollback.')
    logger.info("Trying to set up Chrome browser.")

    try:
        browser = webdriver.Chrome(executable_path=str(config['system']['crome_driver_path']))
    except WebDriverException as e:
        logger.error(str(e))
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']), contents=str(e))
        send_email(email_data=str(e))
        sys.exit(str(config['system']['exit_code']))  # Type valid exit code

    logger.info("Sending the web URL in browser.")
    browser.get(str(config['system']['web_url']))
    browser.maximize_window()

    logger.info("Trying to find the username entity.")
    username_var = None
    username_find_count = 5
    logger.debug("Trying to find username section")
    while not username_var and username_find_count > 0:
        try:
            username_find_count -= 1
            username_var = browser.find_element_by_id('username')
            username_var.send_keys(str(config['system']['login_username']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not username_var and username_find_count <= 0:
        logger.error("Couldn\'t find Username section in login page.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find username field in login page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find username field in login page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Trying to find password entity.")
    password_var = None
    password_find_count = 5
    while not password_var and password_find_count > 0:
        try:
            password_find_count -= 1
            password_var = browser.find_element_by_id('password')
            password_var.send_keys(str(config['system']['login_password']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not password_var and password_find_count <= 0:
        logger.error("Couldn\'t find password section in login page.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find password field in login page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find password field in login page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on login button.")
    login_link = None
    login_link_count = 5
    while login_link_count > 0 and not login_link:
        try:
            login_link_count -= 1
            login_link = browser.find_element_by_name('login')
            login_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if login_link_count <= 0 and not login_link:
        logger.error("Couldn\'t find login button in login page.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find login button link in login page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find login button link in login page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Finding the Home link.")
    home_click = None
    home_click_count = 5
    while home_click_count > 0 and not home_click:
        try:
            home_click_count -= 1
            home_click = browser.find_element_by_id('customHeaderModulePickerBtn-content')
            home_click.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if home_click_count <= 0 and not home_click:
        logger.error("Couldn\'t find home link.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find home button attribute on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find home button attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on Admin Center link.")
    admin_center = None
    admin_center_count = 5
    while not admin_center and admin_center_count > 0:
        try:
            admin_center_count -= 1
            admin_center = browser.find_element_by_link_text('Admin Centre')
            admin_center.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not admin_center and admin_center_count <= 0:
        logger.error("Couldn\'t find link 'Admin center'.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find admin center attribute on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find admin center attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Sending ' Manage Compensation Bucketing Sessions' in the search box.")
    manage_calibration = None
    manage_calibration_count = 5
    while not manage_calibration and manage_calibration_count > 0:
        try:
            manage_calibration_count -= 1
            manage_calibration = browser.find_element_by_id("40_")
            # manage_calibration.send_keys("Manage Calibration Sessions")
            manage_calibration.send_keys("Manage Compensation Bucketing Sessions")
            time.sleep(10)
            manage_calibration.send_keys(Keys.ARROW_DOWN)
            manage_calibration.send_keys(Keys.ENTER)
            time.sleep(5)
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not manage_calibration and manage_calibration_count <= 0:
        logger.error("Couldn\'t send ' Manage Compensation Bucketing Sessions' in the search box.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t validate manage calibration entity on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t validate manage calibration entity on home page')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)
    logger.info("Calling filter function.")
    filter_setup('Setup', browser)
    logger.info("Filter function called Successfully.")

    # check if table exists or not
    logger.info("Trying to find Session list.")
    find_table_full = None
    find_table_full_count = 5
    while not find_table_full and find_table_full_count > 0:
        try:
            time.sleep(10)
            browser.refresh()
            find_table_full_count -= 1
            html = browser.page_source
            soup = BeautifulSoup(html)
            find_table_full = soup.findAll('table', attrs={'class': 'globalTable dataGrid'})
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not find_table_full and find_table_full_count <= 0:
        logger.error("Couldn\'t find link 'session list'.")
        screenshot_name = screenshots(browser, 'session_rollback')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find table contents on manage calibration.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find table contents on manage calibration.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)

    table_var = True
    # table_count = 5
    while table_var:
        try:
            html = browser.page_source
            soup = BeautifulSoup(html)
            # table_count -= 1
            table = soup.findAll('table', attrs={'class': 'globalTable dataGrid'})
        except NoSuchElementException as e:
            time.sleep(5)
            browser.refresh()
        except NoSuchAttributeException as e:
            time.sleep(5)
            browser.refresh()
        except ElementNotVisibleException as e:
            time.sleep(5)
            browser.refresh()
        except Exception as e:
            time.sleep(5)
            browser.refresh()
        # if not table and table_count <= 0:
        # 	browser.save_screenshot('error_screenshot_table.png')
        # 	yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        # 		contents=['Couldn\'t find table contents on manage calibration.', '', screenshot_name])
        # 	sys.exit(str(config['system']['exit_code']))
        try:
            table_data = []
            res = None
            for elem in table:
                tr_elem = elem.findAll("tr", attrs={'class': 'headerRow'})
                if len(tr_elem) == 0:
                    res = elem

            rows = res.findAll("tr")
            for row in rows:
                cells = row.findAll("td")
                cells = [ele.text.strip() for ele in cells]
                table_data.append([ele for ele in cells if ele])
            if not table_data:
                break

            for detail in table_data:
                if detail[2] == 'Setup':
                    session_elem = detail[0]
                    session_crop = session_elem.split('_')
                    session_id = session_crop[1]
                    logger.info("Inserting sessions into the Database.")
                    con = engine.connect()
                    trans = con.begin()
                    rs = con.execute("insert into Processing_Status (Session_Owner_id, session_name) ""VALUES ('" + str(session_id) + "', '" + str(session_elem) + "')")
                    trans.commit()
                    time.sleep(5)
                    logger.info("Trying to find session into the list.")
                    user_linkelem = None
                    user_linkelem_count = 5
                    while not user_linkelem and user_linkelem_count > 0:
                        try:
                            user_linkelem_count -= 1
                            user_linkelem = browser.find_element_by_link_text(session_elem)
                            user_linkelem.click()
                        except NoSuchElementException as e:
                            time.sleep(5)
                            browser.refresh()
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                            browser.refresh()
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                            browser.refresh()
                        except Exception as e:
                            time.sleep(5)
                            browser.refresh()
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not user_linkelem and user_linkelem_count <= 0:
                            logger.error("Couldn\'t find session link elememt in manage calibration page.")
                            screenshot_name = screenshots(browser, 'session_rollback')
                            # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                            #          contents=['Couldn\'t find session link elememt in manage calibration page', '', screenshot_name])
                            send_email(screenshot_name, 'Couldn\'t find session link elememt in manage calibration page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass

                    logger.info("Clicking on Validation button.")
                    validation_link = None
                    validation_link_count = 5
                    while not validation_link and validation_link_count > 0:
                        try:
                            validation_link_count -= 1
                            validation_link = browser.find_element_by_link_text('Validation')
                            validation_link.click()
                        except NoSuchElementException as e:
                            time.sleep(5)
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                        except Exception as e:
                            time.sleep(5)
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not validation_link and validation_link_count <= 0:
                            logger.error("Couldn\'t find ' Validation' button.")
                            screenshot_name = screenshots(browser, 'session_rollback')
                            # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                            #          contents=['Couldn\'t find validation link field in login page', '', screenshot_name])
                            send_email(screenshot_name, 'Couldn\'t find validation link field in login page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass
                    time.sleep(2)
                    logger.info("Clicking on activate button.")
                    span_tag = None
                    span_tag_count = 5
                    while not span_tag and span_tag_count > 0:
                        try:
                            span_tag_count -= 1
                            span_tag = browser.find_elements_by_tag_name('span')
                            for s in span_tag:
                                if s.text.lower().strip() == 'activate':
                                    try:
                                        s.click()
                                        break
                                    except Exception as e:
                                        pass
                        except NoSuchElementException as e:
                            time.sleep(5)
                            browser.refresh()
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                            browser.refresh()
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                            browser.refresh()
                        except Exception as e:
                            time.sleep(5)
                            browser.refresh()
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not span_tag and span_tag_count <= 0:
                            logger.error("Couldn\'t find ' Activate' button.")
                            screenshot_name = screenshots(browser, 'session_rollback')
                            # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                            #          contents=['Couldn\'t find span tag in login page', '', screenshot_name])
                            send_email(screenshot_name, 'Couldn\'t find span tag in login page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass

                    logger.info("Clicking on confirmation pop-up.")
                    validate_link = None
                    validate_link_count = 5
                    while not validate_link and validate_link_count > 0:
                        try:
                            validate_link_count -= 1
                            validate_link = browser.find_element_by_class_name("globalPrimaryButton")
                            validate_link.click()
                            logger.error("session activated")
                        except NoSuchElementException as e:
                            time.sleep(5)
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                        except Exception as e:
                            time.sleep(5)
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not validate_link and validate_link_count <= 0:
                            logger.error("Couldn\'t click on confirmation pop-up.")
                            screenshot_name = screenshots(browser, 'session_rollback')
                            # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                            #          contents=['Couldn\'t find validate link field in login page', '', screenshot_name])
                            send_email(screenshot_name, 'Couldn\'t find validate link field in login page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass

                    logger.info("Updating status of the session into the Database.")
                    con = engine.connect()
                    trans = con.begin()
                    rs = con.execute("update Processing_Status set flag = '1' where session_name = '" + str(session_elem) + "'")
                    trans.commit()
                    logger.info('session activated: ' + str(session_elem))
        except Exception as e:
            try:
                alert_popup = None
                alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                if alert_popup:
                    alert_popup.click()
                else:
                    logger.error("error occured during execution of script.")
                    screenshot_name = screenshots(browser, 'session_rollback')
                    # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                    #          contents=['error occured during execution of script.', '', screenshot_name])
                    send_email(screenshot_name, 'error occured during execution of script.')
                    sys.exit(str(config['system']['exit_code']))
            except Exception as e:
                pass

    logger.info("Successfully completed the rollback function script!")
    browser.quit()

if __name__ == '__main__':
    main_func()
