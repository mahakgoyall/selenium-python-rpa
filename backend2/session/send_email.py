from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email import message
import os
import smtplib
from configparser import RawConfigParser
from logging.config import fileConfig
import configparser
from email.header import Header
from django.core.mail import send_mail as send_email_django

html = """
                    <html><body><p>Dear ////,</p>
                    <p>Compensation Clustering update – following employees cluster is updated at #### stage:</p>
            
                    {table}
                    <p>For further details, please reach out to your manager or HR business partner. </p>
                    <p>Regards,<br>HR Team</p>
                    <p style= "font-style: italic;">This is a system generated email, please do not reply.</p>
                    </body></html>
                    """
# html = html.replace('////', str(manager_name))
# html = html.replace('####', str(manager_cluster_change_stage))
required_column_list = ['Emp ID', 'Emp Name', 'Old Cluster', 'New Cluster', 'Updated by']
# html = html.format(table=df[required_column_list].to_html(index=False))
html = html.replace('<html>',
                    '<html> \n <style>table, th, td {border: 1px solid black ! important;border-collapse: collapse ! important;text-align:left ! important;padding: 8px ! important;}</style>')

# message = MIMEMultipart("alternative", None, [MIMEText(html, 'html')])
message1 = MIMEText(html, 'html')

def send_email(screenshot_name=None, email_data=None, to=None, subject=None, flag=False):

    # configuration file setup
    config = RawConfigParser()
    config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

    print(str(config['mail']['email_smtp']))
    # a= smtplib.SMTP()
    # smtplib.SMTP.login(user='testing.celebal@celebaltech.com', password='divaker@test@12')
    # print(a)
    server = smtplib.SMTP(str(config['mail']['email_smtp']))
    if flag:
        try:
            from django.core.mail import EmailMessage
            subject = 'Compensation Cluster Update'
            msg = EmailMessage(subject, email_data, 'do_not_reply@tatacommunications.com', [to])
            msg.content_subtype = "html"  # Main content is now text/html
            msg.send()
            # send_email_django('email_subject', html, 'do_not_reply@tatacommunications.com', to,
            #               fail_silently=False)
            print("mail sent to " + str(to))
        except Exception as e:
            print(e)
        # server.starttls()
        # server.login('azure_2aae9a184b87ec42ad35dfa18a449a7b@azure.com', 'Celebal@123')
        # email_data['Subject'] = 'Compensation Cluster Update'
        # email_data['From'] = str(Header('Do Not Reply <do_not_reply@tatacommunications.com>'))
        # email_data['To'] = to
        
        # server.sendmail(str(config['mail']['sender_mail']), to, email_data)
        # print(email_data)
        # print(email_data.as_string())
        # m1 = message.Message()
        # m1.add_header('from','me@no.where')
        # m1.add_header('to','myself@some.where')
        # m1.add_header('subject','test')
        # m1.set_payload('test\n')

        # server.sendmail(str(config['mail']['sender_mail']), to, m1.as_string())
        # server.sendmail(str(Header('Do Not Reply <do_not_reply@tatacommunications.com>')), to, email_data.as_string())
        # print('Do Not Reply <do_not_reply@tatacommunications.com>')
        return
    msg = MIMEMultipart()
    msg['Subject'] = str(config['mail']['email_subject'])
    # if type(email_data) is list:
    #     data = MIMEText(email_data[0], "plain")
    #     html_data = MIMEText(email_data[1], "html")
    #     data1 = MIMEText(email_data[2], "plain")
    #     print(data, html_data, data1)
    #     # formatted_data = str(data) + str(html_data) + str(data1)
    #     msg.attach(data)
    #     msg.attach(html_data)
    #     msg.attach(data1)
    #     # msg = msg.as_string()
    # else:
    text = MIMEText(email_data)
    msg.attach(text)

    if screenshot_name:
        ImgFileName = screenshot_name
        with open(ImgFileName, 'rb') as f:
            img_data = f.read()
        image = MIMEImage(img_data, name=os.path.basename(ImgFileName))
        msg.attach(image)
    if to:
        server.send_message(msg, from_addr=str(config['mail']['sender_mail']), to_addrs=to)
    else:
        server.send_message(msg, from_addr=str(config['mail']['sender_mail']),
                       to_addrs=config['mail']['receiver_mail'].split(','))

if __name__ == '__main__':
    send_email(screenshot_name=None, email_data=html, to=['ayush.garg@celebaltech.com'], subject= 'SUBhgfdhgfhb', flag=True)
