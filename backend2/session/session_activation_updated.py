import urllib.parse
import sqlalchemy, collections
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException, ElementNotVisibleException, \
    WebDriverException
from selenium.webdriver.common.keys import Keys
import time
from bs4 import BeautifulSoup
import sys
import os
import cv2
import pytesseract
import logging
from logging.config import fileConfig
from configparser import RawConfigParser
import pandas as pd
import shutil
from time import gmtime, strftime
from datetime import datetime, timedelta
import pytz
from .screenshots import screenshots
from .send_email import send_email
from selenium.webdriver.chrome.options import Options
from .file_download import main_func as fd
from .comment_files_upload import main_func as fu

"""
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
import os
import smtplib

ImgFileName = screenshot_name
with open(ImgFileName, 'rb') as f:
    img_data = f.read()
msg = MIMEMultipart()
msg['Subject'] = str(config['mail']['email_subject'])
text = MIMEText('Couldn\'t find the filter on manage calibration page')
msg.attach(text)
image = MIMEImage(img_data, name=os.path.basename(ImgFileName))
msg.attach(image)
s = smtplib.SMTP(str(config['mail']['email_smtp']))
s.send_message(msg, from_addr=str(config['mail']['sender_mail']), to_addrs=config['mail']['receiver_mail'].split(','))
sys.exit(str(config['system']['exit_code']))
"""

Format_Date = '%Y-%m-%d'
Format_Time = '%H:%M:%S'
ist = pytz.timezone('Asia/Kolkata')

pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files (x86)/Tesseract-OCR/tesseract'


def error_logging():
    fileConfig(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'logging_config.ini'))
    logger = logging.getLogger()
    return logger


# configuration file setup
config = RawConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))


# yagmail setup
# yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
# yag = yagmail.SMTP(str(config['mail']['sender_mail']))
# mail_list = config['mail']['receiver_mail'].split(',')
# mail_list_tuple = tuple((e.strip() for e in mail_list))


def connection_open():
    logger = error_logging()
    logger.info("Database connection function called.")
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) + "};SERVER=" + str(
        config['connection']['server']) + ";DATABASE=" + str(config['connection']['database']) + ";UID=" + str(
        config['connection']['uid']) + ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    logger.info("Database connection function completed.")
    return engine


def create_uploading_files(file_path, level):
    logger = error_logging()
    engine = connection_open()
    try:
        master_df = pd.read_csv(file_path)
    except Exception as e:
        logger.error('Source file not found.' + str(e))

    session_level = ' L' + str(level - 1)
    master_df = master_df[master_df['Session Name'].str.contains(session_level)]

    # mapping of newly created comments file: columns in downloaded file
    col_mappings = {'^UserId': 'Emp Name  User Name', 'Compansationbucket': '', 'comment': 'Comments',
                    'Bywhom': 'Owner User Name', 'Sessionname': 'Session Name'}

    row_index = list(master_df['Comments'].dropna().index)
    master_df = master_df.loc[row_index, :]
    manager_unique_ids = (tuple(set(master_df['Owner User Name'].values)))
    if len(manager_unique_ids) == 1:
        manager_unique_ids = "('" + manager_unique_ids[0] + "')"
    logger.info("manager_unique_ids: " + str(manager_unique_ids))
    print(manager_unique_ids)
    if len(manager_unique_ids) != 0:
        managers_names_df = pd.read_sql_query(
            'select [Username], [First Name], [Last Name] from Employee_central_dump_SF_TCOC where Username in ' + str(
                manager_unique_ids), engine)
    # Loop to create the comments file for upload (incremental load file)
    comment_data = collections.defaultdict(list)
    for index, value in master_df.iterrows():
        for new_column, column in col_mappings.items():
            if new_column in ['^UserId', 'comment', 'Sessionname']:
                comment_data[new_column].append(value[col_mappings[new_column]])
            elif new_column in ['Bywhom']:
                owner_id = value[col_mappings[new_column]]
                manager_df = managers_names_df[managers_names_df['Username'] == owner_id].values
                print(len(manager_df))
                manager_name = (manager_df[0][1] + " " + manager_df[0][2]) if len(manager_df) > 0 else ''
                comment_data[new_column].append(manager_name)
            else:
                comment_data[new_column].append('Compansationbucket')

    comment_dataframe = pd.DataFrame(comment_data)

    date_now = datetime.now(ist).date().isoformat()
    time_now = datetime.now(ist).strftime("%H-%M-%S")

    comment_file_destination = config['files']['destination'] + 'comments/Compansationbucket_comment_' + str(date_now) + str(time_now) + '.csv'
    comment_dataframe.to_csv(comment_file_destination, index=False)

    # Insert userid column in Talent pool file
    talent_pool_df = pd.DataFrame()
    talent_pool_df['^UserId'] = master_df['Emp Name  User Name']

    # Append 'yes' to the userIDs where comment is present
    total_rows = master_df.shape[0]  # count of unique records
    talent_pool_df['talentPool'] = ['Comment'] * total_rows
    comment_flag_file_destination = config['files']['destination'] + 'comments/PersonalInfoData_TataComm Comment Flag_' + str(date_now) + str(time_now) + '.csv'
    talent_pool_df.to_csv(comment_flag_file_destination, index=False)
    logger.info("files created for comments functionality")
    return [comment_flag_file_destination, comment_file_destination]


def filter_setup(status, browser):
    logger = error_logging()
    logger.info("inside filter entity")
    # clear filter before setting it up.
    logger.info("find filter button")
    select_filter_var = None
    select_filter_var_count = 5
    while not select_filter_var and select_filter_var_count > 0:
        try:
            select_filter_var_count -= 1
            select_filter_var = browser.find_elements_by_tag_name('span')
            for span in select_filter_var:
                if span.text.lower().strip() in ['filter', 'filter(1)']:
                    span.click()
                    break
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not select_filter_var and select_filter_var_count <= 0:
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find the filter on manage calibration page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find the filter on manage calibration page')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)

    logger.info("finding the clear button on filter pop-up")
    clear_filter = None
    clear_filter_count = 5
    while not clear_filter and clear_filter_count > 0:
        try:
            clear_filter_count -= 1
            clear_filter = browser.find_element_by_xpath('//button[text()="Clear"]')
            clear_filter.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not clear_filter and clear_filter_count <= 0:
        browser.save_screenshot("error.png")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find clear filter entity in filter pop up.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find clear filter entity in filter pop up.')
        sys.exit(str(config['system']['exit_code']))

    # set filters by setup
    logger.info("Find filter button")
    select_filter_var = None
    select_filter_var_count = 5
    filename = 'filter_screenshot.png'
    browser.save_screenshot(filename)
    image = cv2.imread(filename)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    cv2.imwrite(filename, gray)
    # text = pytesseract.image_to_string(Image.open('D:\\vineet\\calibration\\backend\\'+ filename))
    # text = pytesseract.image_to_string(Image.open(os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))), filename)))
    # os.remove(filename)
    # if "filter" in text.lower():
    #     select_filter_var = False
    while not select_filter_var and select_filter_var_count > 0:
        try:
            select_filter_var_count -= 1
            select_filter_var = browser.find_elements_by_tag_name('span')
            for span in select_filter_var:
                if span.text.lower().strip() in ['filter', 'filter(1)']:
                    span.click()
                    break
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not select_filter_var and select_filter_var_count <= 0:
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find filter on the manage calibration page.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find filter on the manage calibration page.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)
    logger.info("Find drop down on filter pop-up")
    choose_options = None
    choose_options_count = 5
    while not choose_options and choose_options_count > 0:
        try:
            choose_options_count -= 1
            choose_options = browser.find_element_by_class_name('sfMultiSelectButtonIcon')
            choose_options.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not choose_options and choose_options_count <= 0:
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find drop down element on filter pop up.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find drop down element on filter pop up.')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Find status you want to filter in filter entity.")
    find_setup = None
    find_setup_count = 5
    while not find_setup and find_setup_count > 0:
        try:
            find_setup_count -= 1
            find_setup = browser.find_element_by_xpath('//label[text()="' + str(status) + '"]')
            find_setup.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not find_setup and find_setup_count <= 0:
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find setup entity on filter.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find setup entity on filter.')
        sys.exit(str(config['system']['exit_code']))

    logger.info("find apply filter button on filter entity.")
    apply_filter = None
    apply_filter_count = 5
    while not apply_filter and apply_filter_count > 0:
        try:
            apply_filter_count -= 1
            apply_filter = browser.find_element_by_xpath('//button[text()="Apply Filter"]')
            apply_filter.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not apply_filter_count and apply_filter_count <= 0:
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find apply button on filter.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find apply button on filter.')
        sys.exit(str(config['system']['exit_code']))
    logger.info("filter entity completed.")


def main_func(file_no, file):
    start_time = datetime.now(ist)
    logger = error_logging()
    engine = connection_open()
    logger.info("entered in main function of session activation.")
    # return
    # download and update comment flag and L-1 comments of subjects
    if file_no > 1:
        # Upload comment once at GMC level (as there are many files at GMC level)
        con = engine.connect()
        trans = con.begin()
        logger.info("Fetching level number max for comments upload")
        rs = con.execute("select max(current_level) from Processing_Status").fetchall()
        trans.commit()
        level_num = rs[0][0]
        if int(level_num) != 5:
            downloaded_comments_file = fd(file_type='Compensation Clustering Comments')
            files_path = create_uploading_files(file_path=downloaded_comments_file, level=file_no)
            fu(comment_flag_file=files_path[0], comment_file=files_path[1])

    logger.info("set upchrome browser.")
    options = Options()
    options.set_headless(headless=True)
    options.add_argument('--no-sandbox')
    try:
        browser = webdriver.Chrome(executable_path=str(config['system']['crome_driver_path']), chrome_options=options)
        # browser = webdriver.Chrome(executable_path=str(config['system']['crome_driver_path']))
    except WebDriverException as e:
        logger.error(str(e))
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']), contents=str(e))
        send_email(email_data=str(e))
        sys.exit(str(config['system']['exit_code']))  # Type valid exit code

    logger.info("enter web url in browser.")
    browser.get(str(config['system']['web_url']))
    browser.maximize_window()

    logger.info("Trying to find username section")
    time.sleep(2)
    username_var = None
    username_find_count = 5
    while not username_var and username_find_count > 0:
        try:
            username_find_count -= 1
            # browser.execute_script("document.getElementById('username').click()")
            username_var = browser.find_element_by_id('username')
            username_var.send_keys(str(config['system']['login_username']))
            # print(username_var)
            # username_var.send_keys("calib.admin")
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not username_var and username_find_count <= 0:
        logger.error("Couldn\'t find username section.")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find username field in login page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find username field in login page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Trying to find password section")
    password_var = None
    password_find_count = 5
    while not password_var and password_find_count > 0:
        try:
            password_find_count -= 1
            password_var = browser.find_element_by_id('password')
            time.sleep(1)
            password_var.send_keys(str(config['system']['login_password']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)

    if not password_var and password_find_count <= 0:
        logger.error("Couldn\'t find password section.")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find password field in login page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find password field in login page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("clicking on login button.")
    time.sleep(2)
    login_link = None
    login_link_count = 5
    while login_link_count > 0 and not login_link:
        try:
            login_link_count -= 1
            login_link = browser.find_element_by_name('login')
            login_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if login_link_count <= 0 and not login_link:
        logger.error("Couldn\'t find login button.")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find login button link in login page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find login button link in login page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on Home button entity.")
    home_click = None
    home_click_count = 5
    time.sleep(2)
    while home_click_count > 0 and not home_click:
        try:
            home_click_count -= 1
            home_click = browser.find_element_by_id('customHeaderModulePickerBtn-content')
            home_click.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if home_click_count <= 0 and not home_click:
        logger.error("Couldn\'t find HOME button.")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find home button attribute on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find home button attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on Admin Center entity.")
    admin_center = None
    admin_center_count = 5
    while not admin_center and admin_center_count > 0:
        try:
            admin_center_count -= 1
            admin_center = browser.find_element_by_link_text('Admin Centre')
            admin_center.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not admin_center and admin_center_count <= 0:
        logger.error("Couldn\'t find Admin center entity.")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find admin center attribute on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find admin center attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Enter 'Mass Create Compensation Clustering Sessions' in search box entity.")
    create_calibration = None
    create_calibration_count = 5
    while not create_calibration and create_calibration_count > 0:
        try:
            create_calibration_count -= 1
            create_calibration = browser.find_element_by_id("40_")
            # create_calibration.send_keys("Mass Create Compensation Bucketing Sessions")
            create_calibration.send_keys("Mass Create Compensat")
            time.sleep(2)
            create_calibration.send_keys("ion Clustering Sess")
            time.sleep(1)
            create_calibration.send_keys(Keys.ARROW_DOWN)
            print("click arrow down.")
            time.sleep(1)
            create_calibration.send_keys(Keys.ENTER)
            print("press enter.")
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not create_calibration and create_calibration_count <= 0:
        logger.error("Couldn\'t enter 'Mass Create Compensation Clustering Sessions' in search box entity.")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find create calibration entity on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find create calibration entity on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Uploading File for sessions.")
    sendfile_link = None
    sendfile_link_count = 5
    while not sendfile_link and sendfile_link_count > 0:
        try:
            sendfile_link_count -= 1
            sendfile_link = browser.find_element_by_name('fileData1')
            time.sleep(5)
            browser.save_screenshot('2.png')
            sendfile_link.send_keys(str(file))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not create_calibration and create_calibration_count <= 0:
        logger.error("Couldn\'t uploading file for session.")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t  find sendfile link entity on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t  find sendfile link entity on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on Validate sessions button.")
    validatefile_link = None
    validatefile_link_count = 5
    while not validatefile_link and validatefile_link_count > 0:
        try:
            validatefile_link_count -= 1
            validatefile_link = browser.find_element_by_xpath('//*[@value="Validate Import File"]')
            validatefile_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not validatefile_link and validatefile_link_count <= 0:
        logger.error("Couldn\'t validate the file link entity on home page")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t validate the file link entity on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t validate the file link entity on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on Create sessions button.")
    cretesession_link_count = 5
    cretesession_link = None
    time.sleep(2)
    while not cretesession_link and cretesession_link_count > 0:
        try:
            cretesession_link_count -= 1
            cretesession_link = browser.find_element_by_xpath('//*[@value="Create Sessions"]')
            cretesession_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not cretesession_link and cretesession_link_count <= 0:
        logger.error("Couldn\'t validate the create session link entity on home page.")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t validate the create session link entity on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t validate the create session link entity on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("File added successfully.")
    logger.debug("File moving Logic section.")
    date_now = datetime.now(ist).date().isoformat()
    time_now = datetime.now(ist).strftime("%H-%M-%S")
    renamed_file = ".".join(file.split('.')[:-1]) + '_' + str(date_now) + '_' + str(time_now) + '.' + file.split('.')[
        -1]
    shutil.move(file, renamed_file)
    file = renamed_file
    if file_no == 1:
        shutil.move(file, str(config['system']['file_upload_level1_archive']))
    elif file_no == 2:
        shutil.move(file, str(config['system']['file_upload_level2_archive']))
    elif file_no == 3:
        shutil.move(file, str(config['system']['file_upload_level3_archive']))
    elif file_no == 4:
        shutil.move(file, str(config['system']['file_upload_level4_archive']))
    elif file_no == 5:
        shutil.move(file, str(config['system']['file_upload_gmc_archive']))
    logger.debug("File moved to Archieve folder successfully.")

    logger.info("Search admin Center on Home page.")
    adminc_link = None
    adminc_link_count = 5
    time.sleep(5)
    while not adminc_link and adminc_link_count > 0:
        try:
            adminc_link_count -= 1
            adminc_link = browser.find_element_by_class_name('link')
            adminc_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not adminc_link and adminc_link_count <= 0:
        logger.error("Couldn\'t validate admin link entity on home page.")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t validate admin link entity on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t validate admin link entity on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Send 'Manage Compensation Clustering Sessions' key to search box.")
    manage_calibration = None
    manage_calibration_count = 5
    while not manage_calibration and manage_calibration_count > 0:
        try:
            manage_calibration_count -= 1
            manage_calibration = browser.find_element_by_id("40_")
            # manage_calibration.send_keys("Manage Compensation Bucketing Sessions")
            manage_calibration.send_keys("Manage Compensation")
            time.sleep(2)
            manage_calibration.send_keys(" Clustering Sessi")
            time.sleep(2)
            manage_calibration.send_keys(Keys.ARROW_DOWN)
            print("clicking arrow down.")
            time.sleep(1)
            manage_calibration.send_keys(Keys.ENTER)
            print("clicked arrow enter.")
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not manage_calibration and manage_calibration_count <= 0:
        logger.error("Couldn\'t find manage calibration entity on home page.")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find manage calibration entity on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find manage calibration entity on home page')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)
    logger.debug("Filter function called.")
    filter_setup('Setup', browser)
    logger.debug("Filter function called Successfully.")

    logger.info("Checking for session entries.")
    # check if table exists or not
    find_table_full = None
    find_table_full_count = 5
    while not find_table_full and find_table_full_count > 0:
        try:
            time.sleep(10)
            browser.refresh()
            find_table_full_count -= 1
            html = browser.page_source
            soup = BeautifulSoup(html)
            find_table_full = soup.findAll('table', attrs={'class': 'globalTable dataGrid'})
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not find_table_full and find_table_full_count <= 0:
        logger.error("Couldn\'t find table contents on manage calibration.")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find table contents on manage calibration.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find table contents on manage calibration.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)

    table_var = True
    # table_count = 5
    while table_var:
        try:
            html = browser.page_source
            soup = BeautifulSoup(html)
            # table_count -= 1
            table = soup.findAll('table', attrs={'class': 'globalTable dataGrid'})
        except NoSuchElementException as e:
            time.sleep(5)
            browser.refresh()
        except NoSuchAttributeException as e:
            time.sleep(5)
            browser.refresh()
        except ElementNotVisibleException as e:
            time.sleep(5)
            browser.refresh()
        except Exception as e:
            time.sleep(5)
            browser.refresh()
        # if not table and table_count <= 0:
        #   screenshot_name = screenshots(browser, 'session_activation')
        #   yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #       contents=['Couldn\'t find table contents on manage calibration.', '', screenshot_name])
        #   sys.exit(str(config['system']['exit_code']))
        try:
            table_data = []
            res = None
            for elem in table:
                tr_elem = elem.findAll("tr", attrs={'class': 'headerRow'})
                if len(tr_elem) == 0:
                    res = elem

            rows = res.findAll("tr")
            for row in rows:
                cells = row.findAll("td")
                cells = [ele.text.strip() for ele in cells]
                table_data.append([ele for ele in cells if ele])
            if not table_data:
                break

            for detail in table_data:
                if detail[2] == 'Setup':
                    session_elem = detail[0]
                    logger.info('session activating: ' + str(session_elem))
                    session_crop = session_elem.split('_')
                    session_id = session_crop[1]
                    logger.info('session_id: ' + str(session_id))
                    # TODO: unhash
                    try:
                        con = engine.connect()
                        trans = con.begin()
                        logger.info('inside db con')
                        rs = con.execute(
                            "insert into Processing_Status (Session_Owner_id, session_name, current_level) ""VALUES ('" + str(
                                session_id) + "', '" + str(session_elem) + "', '" + str(file_no) + "')")
                        trans.commit()
                    except Exception as e:
                        logger.info("exception in connecting db: " + str(e))
                    time.sleep(5)
                    logger.info("Finding session name in table.")
                    user_linkelem = None
                    user_linkelem_count = 5
                    logger.info("trying to find session_id: " + str(session_id))
                    while not user_linkelem and user_linkelem_count > 0:
                        try:
                            user_linkelem_count -= 1
                            user_linkelem = browser.find_element_by_link_text(session_elem)
                            browser.save_screenshot("session.png")
                            user_linkelem.click()
                        except NoSuchElementException as e:
                            time.sleep(5)
                            browser.refresh()
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                            browser.refresh()
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                            browser.refresh()
                        except Exception as e:
                            time.sleep(5)
                            browser.refresh()
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not user_linkelem and user_linkelem_count <= 0:
                            logger.error("Couldn\'t find session link elememt in manage calibration page.")
                            screenshot_name = screenshots(browser, 'session_activation')
                            # yag.send(to=mail_list_tuple,
                            #          subject=str(config['mail']['email_subject']),
                            #          contents=['Couldn\'t find session link elememt in manage calibration page', '', screenshot_name])
                            send_email(screenshot_name,
                                       'Couldn\'t find session link elememt in manage calibration page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass

                    logger.info("Finding Validation button.")
                    validation_link = None
                    validation_link_count = 5
                    while not validation_link and validation_link_count > 0:
                        try:
                            validation_link_count -= 1
                            validation_link = browser.find_element_by_link_text('Validation')
                            time.sleep(2)
                            browser.save_screenshot("validate.png")
                            validation_link.click()
                            time.sleep(2)
                        except NoSuchElementException as e:
                            time.sleep(5)
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                        except Exception as e:
                            time.sleep(5)
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not validation_link and validation_link_count <= 0:
                            logger.error("Couldn\'t find validation link field in login page.")
                            screenshot_name = screenshots(browser, 'session_activation')
                            # yag.send(to=mail_list_tuple,
                            #          subject=str(config['mail']['email_subject']),
                            #          contents=['Couldn\'t find validation link field in login page', '', screenshot_name])
                            send_email(screenshot_name, 'Couldn\'t find validation link field in login page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass

                    time.sleep(2)
                    logger.info("Finding activate button.")
                    span_tag = None
                    span_tag_count = 5
                    while not span_tag and span_tag_count > 0:
                        try:
                            span_tag_count -= 1
                            span_tag = browser.find_elements_by_tag_name('span')
                            for s in span_tag:
                                if s.text.lower().strip() == 'activate':
                                    try:
                                        time.sleep(2)
                                        s.click()
                                        time.sleep(2)
                                        browser.save_screenshot("activate.png")
                                        break
                                    except Exception as e:
                                        pass
                        except NoSuchElementException as e:
                            time.sleep(5)
                            browser.refresh()
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                            browser.refresh()
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                            browser.refresh()
                        except Exception as e:
                            time.sleep(5)
                            browser.refresh()
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not span_tag and span_tag_count <= 0:
                            logger.error("Couldn\'t find span tag in login page.")
                            screenshot_name = screenshots(browser, 'session_activation')
                            # yag.send(to=mail_list_tuple,
                            #          subject=str(config['mail']['email_subject']),
                            #          contents=['Couldn\'t find span tag in login page', '', screenshot_name])
                            send_email(screenshot_name, 'Couldn\'t find span tag in login page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass

                    logger.info("Clicking on confirmation box.")
                    validate_link = None
                    validate_link_count = 5
                    while not validate_link and validate_link_count > 0:
                        try:
                            validate_link_count -= 1
                            validate_link = browser.find_element_by_class_name("globalPrimaryButton")
                            browser.save_screenshot("validate_link.png")
                            validate_link.click()
                            browser.save_screenshot("validate_link1.png")
                            time.sleep(5)
                            logger.info("session activated.")
                        except NoSuchElementException as e:
                            time.sleep(5)
                        except NoSuchAttributeException as e:
                            time.sleep(5)
                        except ElementNotVisibleException as e:
                            time.sleep(5)
                        except Exception as e:
                            time.sleep(5)
                    try:
                        alert_popup = None
                        alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                        if alert_popup:
                            alert_popup.click()
                        elif not validate_link and validate_link_count <= 0:
                            logger.error("Couldn\'t find validate link field in login page.")
                            screenshot_name = screenshots(browser, 'session_activation')
                            # yag.send(to=mail_list_tuple,
                            #          subject=str(config['mail']['email_subject']),
                            #          contents=['Couldn\'t find validate link field in login page', '', screenshot_name])
                            send_email(screenshot_name, 'Couldn\'t find validate link field in login page')
                            sys.exit(str(config['system']['exit_code']))
                    except Exception as e:
                        pass

                    con = engine.connect()
                    trans = con.begin()
                    rs = con.execute(
                        "update Processing_Status set flag = '1' where session_name = '" + str(session_elem) + "'")
                    trans.commit()
                    logger.info('session activated: ' + str(session_elem))
        except Exception as e:
            try:
                alert_popup = None
                alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                if alert_popup:
                    alert_popup.click()
                else:
                    logger.error("error occurred during execution of script.")
                    screenshot_name = screenshots(browser, 'session_activation')
                    # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                    #          contents=['error occurred during execution of script.', '', screenshot_name])
                    send_email(screenshot_name, 'error occourred during execution of script.')
                    sys.exit(str(config['system']['exit_code']))
            except Exception as e:
                pass
    end_time = datetime.now(ist)
    process_completion_time = (end_time - start_time).seconds
    print(process_completion_time)
    logger.info("Successfully Completed Session activation script with completion time " + str(
        process_completion_time) + "seconds")
    # browser.quit()


if __name__ == "__main__":
    main_func()
    # create_uploading_files('C:/Users/SinghalA/Desktop/calibration/backend/session/files/comments/Compensation_Clustering_Comments_2019_02_22_07_42_17.csv', 2)
    # main_func(1, 'C:\\Users\\SinghalA\\Desktop\\calibration\\backend\\session\\files\\LEVEL1\\level1.csv')
