import sys
import urllib.parse
import sqlalchemy, pytz
import yagmail
import configparser
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from os import listdir
from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException, ElementNotVisibleException, \
    WebDriverException
from selenium.webdriver.common.keys import Keys
import time
from bs4 import BeautifulSoup
import sys
import csv
import os
import logging
from logging.config import fileConfig
from .screenshots import screenshots
from .send_email import send_email
from selenium.webdriver.chrome.options import Options
from datetime import datetime
from .email_updates_new_app import email_updates_func
from .file_download import main_func as file_main

def error_logging():
    fileConfig(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'logging_config.ini'))
    logger = logging.getLogger()
    return logger


# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))
# config.read('config.ini')

# yagmail setup
# yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
# yag = yagmail.SMTP(str(config['mail']['sender_mail']))
# mail_list = config['mail']['receiver_mail'].split(',')
# mail_list_tuple = tuple((e.strip() for e in mail_list))

ist = pytz.timezone('Asia/Kolkata')


def connection_open():
    logger =  error_logging()
    logger.info("Trying to establish Database connection.")
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) + "};SERVER=" + str(
        config['connection']['server']) + ";DATABASE=" + str(config['connection']['database']) + ";UID=" + str(
        config['connection']['uid']) + ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine


def filter_setup(status, browser):
    print(status)
    logger = error_logging()
    logger.info("Trying to find filter entity.")
    time.sleep(5)
    # clear filter before setting it up.
    select_filter_var = None
    select_filter_var_count = 5
    while not select_filter_var and select_filter_var_count > 0:
        try:
            select_filter_var_count -= 1
            select_filter_var = browser.find_element_by_id('SessionList--filterButton-img')
            select_filter_var.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not select_filter_var and select_filter_var_count <= 0:
        screenshot_name = screenshots(browser, 'session_finalizing')
        logger.error("Couldn\'t find filter entity field in filter setup")
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find the filter on manage calibration page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find the filter on manage calibration page')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)
    logger.info("finding the clear button on filter entity")
    clear_filter = None
    clear_filter_count = 5
    while not clear_filter and clear_filter_count > 0:
        try:
            clear_filter_count -= 1
            clear_filter = browser.find_element_by_xpath('//bdi[text()="Clear"]')
            clear_filter.click()
            time.sleep(2)
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not clear_filter and clear_filter_count <= 0:
        browser.save_screenshot("error.png")
        screenshot_name = screenshots(browser, 'session_activation')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find clear filter entity in filter pop up.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find clear filter entity in filter pop up.')
        sys.exit(str(config['system']['exit_code']))
    logger.info("filter clearing while finalization is working fine")

    logger.info("Setting filter status as 'In Progess'.")
    choose_options = None
    choose_options_count = 5
    while not choose_options and choose_options_count > 0:
        try:
            choose_options_count -= 1
            choose_options = browser.find_element_by_id('SessionList--status-inner')
            choose_options.send_keys(str(status))
            time.sleep(2)
            choose_options.send_keys(Keys.ARROW_DOWN)
            choose_options.send_keys(Keys.ENTER)
            time.sleep(2)
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not choose_options and choose_options_count <= 0:
        screenshot_name = screenshots(browser, 'session_finalizing')
        logger.error("Couldn\'t set filter status as 'In Progess' in filter setup")
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find drop down element on filter pop up.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find drop down element on filter pop up.')
        sys.exit(str(config['system']['exit_code']))

    # find_setup = None
    # find_setup_count = 5
    # while not find_setup and find_setup_count > 0:
    #     try:
    #         find_setup_count -= 1
    #         find_setup = browser.find_element_by_xpath('//div[text()="' + str(status) + '"]')
    #         print(find_setup)
    #         find_setup.click()
    #     except NoSuchElementException as e:
    #         time.sleep(2)
    #     except NoSuchAttributeException as e:
    #         time.sleep(2)
    #     except ElementNotVisibleException as e:
    #         time.sleep(2)
    #     except Exception as e:
    #         time.sleep(2)
    # if not find_setup and find_setup_count <= 0:
    #     screenshot_name = screenshots(browser, 'session_finalizing')
    #     logger.error("Couldn\'t set filter status as 'In Progess' in filter setup")
    #     # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
    #     #          contents=['Couldn\'t find setup entity on filter.', '',
    #     #                    screenshot_name])
    #     send_email(screenshot_name, 'Couldn\'t find setup entity on filter.')
    #     sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking the 'Apply Filter' button to save changes.")
    apply_filter = None
    apply_filter_count = 5
    while not apply_filter and apply_filter_count > 0:
        try:
            apply_filter_count -= 1
            apply_filter = browser.find_element_by_id('__button6-content')
            apply_filter.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not apply_filter_count and apply_filter_count <= 0:
        screenshot_name = screenshots(browser, 'session_finalizing')
        logger.error("Couldn\'t click on 'Apply Filter' in filter setup")
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find apply button on filter.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find apply button on filter.')
        sys.exit(str(config['system']['exit_code']))


def guidelines_setting(browser, flag_value):

    logger = error_logging()
    logger.info("Finding status of guidelines from DB.")
    engine = connection_open()
    con = engine.connect()
    trans = con.begin()
    rs = con.execute("select flag from Finalizing_Status where session_name = 'guidelines'").fetchall()
    trans.commit()
    print('response db:', rs[0][0])
    if str(rs[0][0]) == '0':
        logger.info('inside zero guidelines condition. No need to go through guidelines process steps.')
        return

    logger.info("Clicking on 'Admin Center' entity.")
    admin_center_count = 5
    admin_center = None
    while not admin_center and admin_center_count > 0:
        try:
            time.sleep(2)
            admin_center_count -= 1
            admin_center = browser.find_element_by_link_text('Admin Centre')
            time.sleep(2)
            admin_center.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not admin_center and admin_center_count <= 0:
        screenshot_name = screenshots(browser, 'session_finalizing')
        logger.error("Couldn\'t click on 'Admin Center' in guidelines_setting")
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find home button attribute on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find home button attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Send 'Manage Compensation Clustering Templates' string to search box.")
    manage_templates_count = 5
    manage_templates = None
    while not manage_templates and manage_templates_count > 0:
        try:
            manage_templates_count -= 1
            manage_templates = browser.find_element_by_id("40_")
            manage_templates.send_keys("Manage Compensation ")
            time.sleep(2)
            manage_templates.send_keys("Clustering Templ")
            time.sleep(2)
            manage_templates.send_keys(Keys.ARROW_DOWN)
            manage_templates.send_keys(Keys.ENTER)
            time.sleep(2)
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not manage_templates and manage_templates_count <= 0:
        screenshot_name = screenshots(browser, 'session_finalizing')
        logger.error("Couldn\'t send keys 'Manage Compensation Clustering Templates' to search box in guidelines_setting")
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find manage templates attribute on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find manage templates attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on V2 bucket.")
    template_v2_count = 5
    template_v2 = None
    while not template_v2 and template_v2_count > 0:
        try:
            template_v2_count -= 1
            template_v2 = browser.find_element_by_link_text('Compensation Clustering - V2')
            template_v2.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not template_v2 and template_v2_count <= 0:
        screenshot_name = screenshots(browser, 'session_finalizing')
        logger.error("Couldn\'t find element 'Compensation Clustering - V2' in guidelines_setting")
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find Compensation bucket V2 attribute on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find Compensation Clustering V2 attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on 'Advanced' Tab.")
    advanced_button_count = 5
    advanced_button = None
    while not advanced_button and advanced_button_count > 0:
        try:
            advanced_button_count -= 1
            advanced_button = browser.find_element_by_link_text('Advanced')
            advanced_button.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not advanced_button and advanced_button_count <= 0:
        screenshot_name = screenshots(browser, 'session_finalizing')
        logger.error("Couldn\'t find element 'Advanced' in guidelines_setting")
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find advanced button on manage templates page.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find advanced button on manage templates page.')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on checkbox for guidelines settings.")
    guidelines_checkbox_count = 5
    guidelines_checkbox = None
    while not guidelines_checkbox and guidelines_checkbox_count > 0:
        try:
            guidelines_checkbox_count -= 1
            guidelines_checkbox = browser.find_element_by_xpath('//label[text()="Enable Guidelines Enforcement"]')
            guidelines_checkbox.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not guidelines_checkbox and guidelines_checkbox_count <= 0:
        screenshot_name = screenshots(browser, 'session_finalizing')
        logger.error("Couldn\'t find checkbox in guidelines_setting")
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find guildlines checkbox on advanced tab in templates page.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find guildlines checkbox on advanced tab in templates page.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(3)
    logger.info("Clicking on save button to save the  changes.")
    save_btn_count = 5
    save_btn = None
    while not save_btn and save_btn_count > 0:
        try:
            save_btn_count -= 1
            save_btn = browser.find_elements_by_tag_name('span')
            for span in save_btn:
                if span.text.lower().strip() == 'save':
                    span.click()
                    logger.info("Saving the status in Database.")
                    con = engine.connect()
                    trans = con.begin()
                    con.execute("update Finalizing_Status set flag = '" + str(flag_value) + "' where session_name = 'guidelines'")
                    trans.commit()
                    logger.info("guidlines flag set to " + str(flag_value))
                    break
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not save_btn and save_btn_count <= 0:
        screenshot_name = screenshots(browser, 'session_finalizing')
        logger.error("Couldn\'t click on save button in guidelines_setting")
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find save button on advanced tab in templates page.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find save button on advanced tab in templates page.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(3)
    logger.info("Trying if confirmation pop-up appears")
    logger.info("Clicking on the confirmation pop-up.")
    try:
        confirm_btn_count = 5
        confirm_btn = None
        while not confirm_btn and confirm_btn_count > 0:
            try:
                confirm_btn_count -= 1
                confirm_btn = browser.find_element_by_class_name(' globalPrimaryButton')
                confirm_btn.click()
            except NoSuchElementException as e:
                time.sleep(2)
            except NoSuchAttributeException as e:
                time.sleep(2)
            except ElementNotVisibleException as e:
                time.sleep(2)
            except Exception as e:
                time.sleep(2)
        if not confirm_btn and confirm_btn_count <= 0:
            pass
            # screenshot_name = screenshots(browser, 'session_finalizing')
            # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
            #          contents=['Couldn\'t find confirm button on pop up of advanced tab save button in templates page.', '', screenshot_name])
            # send_email(screenshot_name, 'Couldn\'t find confirm button on pop up of advanced tab save button in templates page.')
            # sys.exit(str(config['system']['exit_code']))
    except Exception as e:
        logger.error("confirmation box didn't appear. Trying to continue.")

    time.sleep(3)
    logger.info("Clicking on Admin link.")
    admin_linkc_count = 5
    admin_linkc = None
    while not admin_linkc and admin_linkc_count > 0:
        try:
            admin_linkc_count -= 1
            admin_linkc = browser.find_element_by_id('customHeaderModulePickerBtn-content')
            admin_linkc.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not admin_linkc and admin_linkc_count <= 0:
        screenshot_name = screenshots(browser, 'session_finalizing')
        logger.error("Couldn\'t find element 'customHeaderModulePickerBtn-content' in guidelines_setting")
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find admin link on templates page.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find admin link on templates page.')
        sys.exit(str(config['system']['exit_code']))


def main_func():

    start_time = datetime.now(ist)
    logger = error_logging()
    engine = connection_open()
    logger.info("Entered in main function of session finalizing.")

    # Headless Chrome setup
    options = Options()
    options.set_headless(headless=True)
    options.add_argument("--log-level=OFF")
    try:
        # browser = webdriver.Chrome(executable_path=str(config['system']['crome_driver_path']))
        browser = webdriver.Chrome(executable_path=str(config['system']['crome_driver_path']), chrome_options=options)
    except WebDriverException as e:
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=str(e))
        send_email(email_data=str(e))
        sys.exit(str(config['system']['exit_code']))  # Type valid exit code

    logger.info("Sending the URL to the browser.")
    browser.get(str(config['system']['web_url']))
    browser.maximize_window()

    logger.info("Trying to find username entity.")
    username_var = None
    username_find_count = 5
    logger.debug("Trying to find username section")
    while username_find_count > 0 and not username_var:
        try:
            username_find_count -= 1
            username_var = browser.find_element_by_id('username')
            username_var.send_keys(str(config['system']['login_username']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not username_var and username_find_count <= 0:
        logger.error("Couldn\'t find username field in login page")
        screenshot_name = screenshots(browser, 'session_finalizing')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find username field in login page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find username field in login page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Trying to find password entity.")
    password_var = None
    password_find_count = 5
    while password_find_count > 0 and not password_var:
        try:
            password_find_count -= 1
            password_var = browser.find_element_by_id('password')
            password_var.send_keys(str(config['system']['login_password']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not password_var and password_find_count <= 0:
        logger.error("Couldn\'t find password field in login page")
        screenshot_name = screenshots(browser, 'session_finalizing')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find password field in login page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find password field in login page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Clicking on Login button.")
    login_link_count = 5
    login_link = None
    while login_link_count > 0 and not login_link:
        try:
            login_link_count -= 1
            login_link = browser.find_element_by_name('login')
            login_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if login_link_count <= 0 and not login_link:
        logger.error("Couldn\'t find lomgin button in login page")
        screenshot_name = screenshots(browser, 'session_finalizing')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find login button link in login page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find login button link in login page')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(5)
    logger.info("Clicking on Home button link.")
    home_click_count = 5
    home_click = None
    while home_click_count > 0 and not home_click:
        try:
            home_click_count -= 1
            home_click = browser.find_element_by_id('customHeaderModulePickerBtn-content')
            home_click.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if home_click_count <= 0 and not home_click:
        logger.error("Couldn\'t find HOME button")
        screenshot_name = screenshots(browser, 'session_finalizing')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find home button attribute on home page', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find home button attribute on home page')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Calling guidelines function.")
    guidelines_setting(browser, '0')
    logger.info("Successfully called guidelines function.")

    time.sleep(3)
    logger.info("Clicking on 'Compensation Clustering' link.")
    calibration_link_count = 5
    calibration_link = None
    while not calibration_link and calibration_link_count > 0:
        try:
            calibration_link_count -= 1
            calibration_link = browser.find_element_by_link_text('Compensation Clustering')
            calibration_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not calibration_link and calibration_link_count <= 0:
        logger.error("Couldn\'t find link Compensation Clustering ")
        screenshot_name = screenshots(browser, 'session_finalizing')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find Compensation Bucketing attribute in drop down list.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find Compensation Clustering attribute in drop down list.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)
    logger.info("Calling Filter function.")
    filter_setup('In Progress', browser)
    logger.info("Successsfully called Filter entity.")

    # check if table exists or not
    logger.info("Checking for the sessions list.")
    find_table_full = None
    find_table_full_count = 5
    while not find_table_full and find_table_full_count > 0:
        try:
            time.sleep(10)
            browser.refresh()
            find_table_full_count -= 1
            html = browser.page_source
            soup = BeautifulSoup(html)
            find_table_full = soup.find('table', attrs={'id': 'SessionList--sessionListTable-table-fixed'})
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not find_table_full and find_table_full_count <= 0:
        logger.error("Couldn\'t find element 'SessionList--sessionListTable-table-fixed' in session list ")
        screenshot_name = screenshots(browser, 'session_finalizing')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find table contents on calibration page.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find table contents on calibration page.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(2)
    table = True
    flag = False
    while table and not flag:
        try:
            html = browser.page_source
            soup = BeautifulSoup(html)
            table = soup.find('table', attrs={'id': 'SessionList--sessionListTable-table-fixed'})
        except NoSuchElementException as e:
            time.sleep(5)
            browser.refresh()
        except NoSuchAttributeException as e:
            time.sleep(5)
            browser.refresh()
        except ElementNotVisibleException as e:
            time.sleep(5)
            browser.refresh()
        except Exception as e:
            time.sleep(5)
            browser.refresh()
        if not table:
            table = False
        # if not table and table_count <= 0:
        # 	browser.save_screenshot('error_screenshot_closing_table.png')
        # 	yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        # 		contents=['Couldn\'t find table contents on calibration page.', '', screenshot_name])
        # 	sys.exit(str(config['system']['exit_code']))

        table_data = []
        res = None
        for elem in table:
            tr_elem = elem.find("tr", attrs={'id': 'SessionList--sessionListTable-rows-row0-fixed'})
            res = elem

        for row in res:
            cells = row.findAll("td", attrs={'id': 'SessionList--sessionListTable-rows-row0-col1'})
            cells = [ele.text.strip() for ele in cells]
            table_data.append([ele for ele in cells if ele])
        table_data2 = [x for x in table_data if x != []]
        if table_data2 == list():
            logger.info("Updating guidelines in Database")
            engine = connection_open()
            con = engine.connect()
            trans = con.begin()
            rs = con.execute("update Finalizing_Status set flag = 'null' where session_name = 'guidelines'")
            trans.commit()
            flag = True
        else:
            flag = False
        if flag:
            table = False

        for detail in table_data2:
            if detail != list():
                session_elem = detail[0]
                session_crop = session_elem.split('_')
                session_id = session_crop[1]
                logger.info("Inserting sessions into DB for logging.")
                con = engine.connect()
                trans = con.begin()
                rs = con.execute("insert into Finalizing_Status  (Session_Owner_id, session_name) " "VALUES ('" + str(session_id) + "', '" + str(session_elem) + "')")
                trans.commit()
                time.sleep(5)
                logger.info("Finding session to Finalize.")
                user_linkelem = None
                user_linkelem_count = 5
                while not user_linkelem and user_linkelem_count > 0:
                    try:
                        user_linkelem_count -= 1
                        user_linkelem = browser.find_element_by_link_text(session_elem)
                        user_linkelem.click()
                    except NoSuchElementException as e:
                        time.sleep(2)
                    except NoSuchAttributeException as e:
                        time.sleep(2)
                    except ElementNotVisibleException as e:
                        time.sleep(2)
                    except Exception as e:
                        time.sleep(2)
                try:
                    alert_popup = None
                    alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                    if alert_popup:
                        alert_popup.click()
                    elif not user_linkelem and user_linkelem_count <= 0:
                        logger.error("Couldn\'t find element session to Finalize ")
                        screenshot_name = screenshots(browser, 'session_finalizing')
                        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                        #          contents=['Couldn\'t find user link elememt in login page', '', screenshot_name])
                        send_email(screenshot_name, 'Couldn\'t find user link elememt in login page')
                        sys.exit(str(config['system']['exit_code']))
                except Exception as e:
                    pass
                
                logger.info("Clicking on Finalize button.")
                time.sleep(15)
                finalize_session = None
                finalize_session_count = 5
                while not finalize_session and finalize_session_count > 0:
                    try:
                        finalize_session_count -= 1
                        finalize_session = browser.find_element_by_id('SessionView--finalizeButton-content')
                        if not finalize_session:
                            print("inside if condition.....")
                            time.sleep(10)
                            finalize_session = browser.find_element_by_id('SessionView--finalizeButton-content')
                            # continue
                        print("trying to click on finalizing button....")
                        finalize_session.click()
                            
                    except NoSuchElementException as e:
                        browser.refresh()
                        time.sleep(5)
                    except NoSuchAttributeException as e:
                        browser.refresh()
                        time.sleep(5)
                    except ElementNotVisibleException as e:
                        browser.refresh()
                        time.sleep(5)
                    except Exception as e:
                        browser.refresh()
                        time.sleep(5)
                try:
                    alert_popup = None
                    alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                    if alert_popup:
                        alert_popup.click()
                    elif not finalize_session and finalize_session_count <= 0:
                        logger.error("Couldn\'t find Finalize button")
                        screenshot_name = screenshots(browser, 'session_finalizing')
                        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                        #          contents=['Couldn\'t find span tag in login page', '', screenshot_name])
                        send_email(screenshot_name, 'Couldn\'t find finalize button on page.')
                        sys.exit(str(config['system']['exit_code']))
                except Exception as e:
                    pass

                time.sleep(8)
                logger.info("Clicking on confirmation pop-up.")
                confirm_box_count = 5
                confirm_box = None
                while not confirm_box and confirm_box_count > 0:
                    try:
                        time.sleep(3)
                        confirm_box_count -= 1
                        confirm_box = browser.find_element_by_xpath('//bdi[text()="Yes"]')
                        confirm_box.click()
                        con = engine.connect()
                        trans = con.begin()
                        rs = con.execute("insert into Finalizing_Status  (Session_Owner_id, session_name) " "VALUES ('" + str(session_id) + "', '" + str(session_elem) + "')")
                        trans.commit()
                    except NoSuchElementException as e:
                        time.sleep(2)
                    except NoSuchAttributeException as e:
                        time.sleep(2)
                    except ElementNotVisibleException as e:
                        time.sleep(2)
                    except Exception as e:
                        time.sleep(2)
                try:
                    alert_popup = None
                    alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                    if alert_popup:
                        alert_popup.click()
                    elif not confirm_box and confirm_box_count <= 0:
                        logger.error("Couldn\'t find confirmation pop-up.")
                        screenshot_name = screenshots(browser, 'session_finalizing')
                        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                        #          contents=['Couldn\'t find confirmation box elements on confirmation pop-up.', '', screenshot_name])
                        send_email(screenshot_name, 'Couldn\'t find confirmation box elements on confirmation pop-up.')
                        sys.exit(str(config['system']['exit_code']))
                except Exception as e:
                    pass

                logger.info("Clicking on 'session list' link to go back on Finalize page.")
                session_list_count = 5
                session_list = None
                
                while not session_list and session_list_count > 0:
                    try:
                        time.sleep(10)
                        session_list_count -= 1
                        session_list = browser.find_element_by_link_text('Session List')
                        session_list.click()
                    except NoSuchElementException as e:
                        browser.refresh()
                        time.sleep(10)
                    except NoSuchAttributeException as e:
                        browser.refresh()
                        time.sleep(10)
                    except ElementNotVisibleException as e:
                        browser.refresh()
                        time.sleep(10)
                    except Exception as e:
                        browser.refresh()
                        time.sleep(10)
                try:
                    alert_popup = None
                    alert_popup = browser.find_element_by_xpath('//button[text()="Keep Working"]')
                    if alert_popup:
                        alert_popup.click()
                    elif not session_list and session_list_count <= 0:
                        logger.error("Couldn\'t find session list.")
                        screenshot_name = screenshots(browser, 'session_finalizing')
                        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
                        #          contents=['Couldn\'t find session list link on the page.', '', screenshot_name])
                        send_email(screenshot_name, 'Couldn\'t find session list link on the page.')
                        sys.exit(str(config['system']['exit_code']))
                except Exception as e:
                    pass

    time.sleep(2)
    logger.info("Clicking on 'Compensation Clustering' link.")
    calibration_linkc_count = 5
    calibration_linkc = None
    while not calibration_linkc and calibration_linkc_count > 0:
        try:
            calibration_linkc_count -= 1
            calibration_linkc = browser.find_element_by_id('customHeaderModulePickerBtn-content')
            calibration_linkc.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not calibration_linkc and calibration_linkc_count <= 0:
        logger.error("Couldn\'t find link Compensation Clustering.")
        screenshot_name = screenshots(browser, 'session_finalizing')
        # yag.send(to=mail_list_tuple, subject=str(config['mail']['email_subject']),
        #          contents=['Couldn\'t find Compensation Bucketing link on the page.', '', screenshot_name])
        send_email(screenshot_name, 'Couldn\'t find Compensation Clustering link on the page.')
        sys.exit(str(config['system']['exit_code']))

    logger.info("Calling guidelines function.")
    guidelines_setting(browser, 'null')
    logger.info("Successfully called guidelines function.")
    browser.quit()
    #update flag value after finalizing of sessions to start email updates
    con = engine.connect()
    trans = con.begin()
    rs = con.execute("update Finalizing_Status set flag = '1' where session_name = 'email_updates'")
    trans.commit()

    end_time = datetime.now(ist)
    process_completion_time = (end_time - start_time).seconds
    logger.info("Successfully Completed Session finalization script with completion time " + str(process_completion_time) + "seconds")
    logger.info("started executing email updates function")
    destination = str(config['files']['destination'])
    hierarchy_file = destination + 'hierarchy/hierarchy_file.csv'
    audit_file = file_main(file_type='Calibration_Audit_2019')
    print(audit_file, "audit_file")
    email_updates_func(audit_report=audit_file, hierarchy_file=hierarchy_file)
    logger.info("successfully completed email updates script")



if __name__ == '__main__':
    main_func()
